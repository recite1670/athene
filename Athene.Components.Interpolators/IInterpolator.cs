using System.Collections.Generic;

namespace Athene.Components.Interpolators
{
	/// <summary>
	///		Responsible for interpolating any segments of characters.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public interface IInterpolator<in TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	Interpolates a segment of characters.
		/// </summary>
		/// <param name="raw">The raw characters to be interpolated.</param>
		/// <param name="context">The currently running execution context.</param>
		/// <returns>The interpolated characters.</returns>
		IEnumerable<char> Interpolate(IEnumerable<char> raw, TContext context);
	}
}
