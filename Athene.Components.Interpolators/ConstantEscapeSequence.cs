using System;
using System.Collections.Generic;

namespace Athene.Components.Interpolators
{
	/// <summary>
	/// 	An escape sequence that is a constant set of characters.
	/// </summary>
	public class ConstantEscapeSequence<TContext> : EscapeSequence<TContext> where TContext : IExecutionContext
	{
		private readonly IEnumerable<char> _escaped;

		/// <summary>
		/// 	Constructs an instance of <see cref="ConstantEscapeSequence{TContext}"/>.
		/// </summary>
		/// <param name="unescaped">The sequence to convert from.</param>
		/// <param name="escaped">The sequence to convert to.</param>
		public ConstantEscapeSequence(IEnumerable<char> unescaped, IEnumerable<char> escaped) : base(unescaped)
		{
			_escaped = escaped ?? throw new ArgumentNullException(nameof(escaped));
		}

		/// <inheritdoc cref="EscapeSequence{TContext}.Escape"/>
		protected override IEnumerable<char> Escape(IEnumerator<char> raw, TContext context)
		{
			return _escaped;
		}
	}
}
