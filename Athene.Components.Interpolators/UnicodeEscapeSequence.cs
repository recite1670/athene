using System;
using System.Collections.Generic;
using System.Globalization;
using Athene.Extensions;

namespace Athene.Components.Interpolators
{
	/// <summary>
	/// 	An escape sequence that represents the C# escape code <code>\u_ _ _ _</code>.
	/// </summary>
	public class UnicodeEscapeSequence<TContext> : EscapeSequence<TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	Constructs an instance of <see cref="UnicodeEscapeSequence{TContext}"/>.
		/// </summary>
		/// <param name="startSequence">The start of the escape sequence.</param>
		public UnicodeEscapeSequence(IEnumerable<char> startSequence) : base(startSequence)
		{
		}

		/// <inheritdoc cref="EscapeSequence{TContext}.Escape"/>
		/// <exception cref="ArgumentException">The hex number is invalid.</exception>
		protected override IEnumerable<char> Escape(IEnumerator<char> raw, TContext context)
		{
			if (!short.TryParse(new string(raw.TakeToArray(4)), NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out short value))
			{
				throw new ArgumentException("Hex value was invalid.", nameof(raw));
			}

			return new[]
			{
				(char) value
			};
		}
	}
}
