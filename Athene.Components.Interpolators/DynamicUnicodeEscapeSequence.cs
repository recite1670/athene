using System;
using System.Collections.Generic;
using System.Globalization;
using Athene.Extensions;

namespace Athene.Components.Interpolators
{
	/// <summary>
	/// 	An escape sequence that represents the C# escape code <code>\x_ _ _ _</code>.
	/// </summary>
	public class DynamicUnicodeEscapeSequence<TContext> : EscapeSequence<TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	Constructs an instance of <see cref="DynamicUnicodeEscapeSequence{TContext}"/>.
		/// </summary>
		/// <param name="startSequence">The start of the escape sequence.</param>
		public DynamicUnicodeEscapeSequence(IEnumerable<char> startSequence) : base(startSequence)
		{
		}

		/// <inheritdoc cref="EscapeSequence{TContext}.Escape"/>
		/// <exception cref="ArgumentException">A digit of the hex number is invalid.</exception>
		protected override IEnumerable<char> Escape(IEnumerator<char> raw, TContext context)
		{
			const string invalidDigitMessage = "The hex digit at index {0} was invalid.";
			if (!byte.TryParse(new string(raw.TakeNext(), 1), NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte digitValue))
			{
				throw new ArgumentException(string.Format(invalidDigitMessage, 0), nameof(raw));
			}

			int value = digitValue;
			for (int i = 1; i < 4; ++i)
			{
				if (!raw.TryTakeNext(out char nextChar))
				{
					break;
				}

				if (!byte.TryParse(new string(nextChar, 1), NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out digitValue))
				{
					return new[]
					{
						(char) value,
						nextChar
					};
				}

				// * 16 to move the previous hex value one digit left.
				value = value * 16 + digitValue;
			}

			return new[]
			{
				(char) value
			};
		}
	}
}
