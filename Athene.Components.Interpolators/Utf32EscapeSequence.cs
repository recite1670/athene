using System;
using System.Collections.Generic;
using System.Globalization;
using Athene.Extensions;

namespace Athene.Components.Interpolators
{
	/// <summary>
	/// 	An escape sequence that represents the C# escape code <code>\U_ _ _ _ _ _ _ _</code>.
	/// </summary>
	public class Utf32EscapeSequence<TContext> : EscapeSequence<TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	Constructs an instance of <see cref="Utf32EscapeSequence{TContext}"/>.
		/// </summary>
		/// <param name="startSequence">The start of the escape sequence.</param>
		public Utf32EscapeSequence(IEnumerable<char> startSequence) : base(startSequence)
		{
		}

		/// <inheritdoc cref="EscapeSequence{TContext}.Escape"/>
		/// <exception cref="ArgumentException">The hex number is invalid.</exception>
		protected override IEnumerable<char> Escape(IEnumerator<char> raw, TContext context)
		{
			string rawHex = new string(raw.TakeToArray(8));
			if (!int.TryParse(rawHex, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out int value))
			{
				throw new ArgumentException("Hex value was invalid.", nameof(raw));
			}

			return new[]
			{
				(char) value
			};
		}
	}
}
