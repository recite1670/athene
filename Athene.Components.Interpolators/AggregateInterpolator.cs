using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Athene.Components.Interpolators
{
	/// <summary>
	/// 	An interpolator that pipes the results of one interpolator into the next.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public class AggregateInterpolator<TContext> : IInterpolator<TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	The default C# escape characters.
		/// </summary>
		public static IEnumerable<IInterpolator<TContext>> DefaultEscapeCharacters { get; } = new ReadOnlyCollection<IInterpolator<TContext>>(new IInterpolator<TContext>[]
		{
			new ConstantEscapeSequence<TContext>(new[] { '\\', '\'' }, new[] { '\'' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', '"' }, new[] { '\"' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', '\\' }, new[] { '\\' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', '0' }, new[] { '\0' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', 'a' }, new[] { '\a' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', 'b' }, new[] { '\b' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', 'f' }, new[] { '\f' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', 'n' }, new[] { '\n' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', 'r' }, new[] { '\r' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', 't' }, new[] { '\t' }),
			new ConstantEscapeSequence<TContext>(new[] { '\\', 'v' }, new[] { '\v' }),
			new UnicodeEscapeSequence<TContext>(new[] { '\\', 'u' }),
			new DynamicUnicodeEscapeSequence<TContext>(new[] { '\\', 'x' }),
			new Utf32EscapeSequence<TContext>(new[] { '\\', 'U' })
		});

		private IEnumerable<IInterpolator<TContext>> _interpolators;

		/// <summary>
		/// 	The interpolators to aggregate.
		/// </summary>
		public IEnumerable<IInterpolator<TContext>> Interpolators
		{
			get => _interpolators;
			set => _interpolators = value ?? throw new ArgumentNullException(nameof(value));
		}

		/// <summary>
		/// 	Constructs an instance of <see cref="AggregateInterpolator{TContext}"/> with <seealso cref="DefaultEscapeCharacters"/> as the interpolators.
		/// </summary>
		public AggregateInterpolator() : this(DefaultEscapeCharacters)
		{
		}

		/// <summary>
		/// 	Constructs an instance of <see cref="AggregateInterpolator{TContext}"/>.
		/// </summary>
		/// <param name="interpolators">The interpolators to aggregate.</param>
		/// <exception cref="ArgumentNullException"><paramref name="interpolators"/> is <see langword="null"/>.</exception>
		public AggregateInterpolator(IEnumerable<IInterpolator<TContext>> interpolators)
		{
			_interpolators = interpolators ?? throw new ArgumentNullException(nameof(interpolators));
		}

		/// <inheritdoc cref="IInterpolator{TContext}.Interpolate"/>
		public IEnumerable<char> Interpolate(IEnumerable<char> raw, TContext context)
		{
			return _interpolators.Aggregate(raw, (current, interpolator) => interpolator.Interpolate(current, context));
		}
	}
}
