using System;
using System.Collections.Generic;

namespace Athene.Components.Interpolators
{
	/// <summary>
	/// 	An interpolator that replaces a sequence of characters that have a starting pattern (unescaped characters) with escaped characters.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public abstract class EscapeSequence<TContext> : IInterpolator<TContext> where TContext : IExecutionContext
	{
		private readonly IEnumerable<char> _startSequence;

		/// <summary>
		/// 	Constructs an instance of <see cref="EscapeSequence{TContext}"/>.
		/// </summary>
		/// <param name="startSequence">The starting pattern.</param>
		protected EscapeSequence(IEnumerable<char> startSequence)
		{
			_startSequence = startSequence ?? throw new ArgumentNullException(nameof(startSequence));
		}

		/// <summary>
		/// 	Converts any remaining data needed into escaped characters.
		/// </summary>
		/// <param name="raw">The remaining unescaped content.</param>
		/// <param name="context">The currently running execution context.</param>
		/// <returns>The escaped characters from the advanced enumerator.</returns>
		protected abstract IEnumerable<char> Escape(IEnumerator<char> raw, TContext context);

		/// <inheritdoc cref="IInterpolator{TContext}.Interpolate"/>
		/// <exception cref="ArgumentNullException"><paramref name="raw"/> is <see langword="null"/>.</exception>
		public IEnumerable<char> Interpolate(IEnumerable<char> raw, TContext context)
		{
			if (raw == null)
			{
				throw new ArgumentNullException(nameof(raw));
			}

			using (IEnumerator<char> startSequenceEnumerator = _startSequence.GetEnumerator())
			{
				if (!startSequenceEnumerator.MoveNext())
				{
					foreach (char c in raw)
					{
						yield return c;
					}

					yield break;
				}

				List<char> currentSequence = new List<char>();

				using (IEnumerator<char> rawEnumerator = raw.GetEnumerator())
				{
					while (rawEnumerator.MoveNext())
					{
						char currentChar = rawEnumerator.Current;

						if (currentChar == startSequenceEnumerator.Current)
						{
							if (!startSequenceEnumerator.MoveNext())
							{
								foreach (char escaped in Escape(rawEnumerator, context))
								{
									yield return escaped;
								}

								currentSequence.Clear();

								startSequenceEnumerator.Reset();
								startSequenceEnumerator.MoveNext();
							}
							else
							{
								currentSequence.Add(currentChar);
							}
						}
						else
						{
							foreach (char sequenceChar in currentSequence)
							{
								yield return sequenceChar;
							}

							yield return currentChar;

							currentSequence.Clear();

							startSequenceEnumerator.Reset();
							startSequenceEnumerator.MoveNext();
						}
					}
				}
			}
		}
	}
}
