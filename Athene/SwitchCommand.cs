﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Athene.Tables;

namespace Athene
{
	/// <summary>
	///     A command that would switch execution to a child command.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public class SwitchCommand<TContext> : ICommand<TContext> where TContext : IExecutionContext
	{
		private static void RecursiveCommandAppend(KeyValuePair<Aliases, ICommand<TContext>> command, StringBuilder list, StringBuilder source)
		{
			SwitchCommand<TContext> switchCommand = command.Value as SwitchCommand<TContext>;

			string aliases = null;
			if (switchCommand == null || switchCommand.DefaultChild != null)
			{
				aliases = command.Key.ToString();

				list.AppendLine();

				// Hanging indent.
				list.Append("    - ");
				list.Append(source);
				list.Append(aliases);
			}

			if (switchCommand == null)
			{
				list.Append(' ');
				list.Append(command.Value.Signature);
			}
			else
			{
				aliases = aliases ?? command.Key.ToString();

				int sourceLength = aliases.Length + 1;
				source.Append(command.Key);
				source.Append(' ');

				foreach (KeyValuePair<Aliases, ICommand<TContext>> childCommand in switchCommand.Children)
				{
					RecursiveCommandAppend(childCommand, list, source);
				}

				source.Remove(source.Length - sourceLength, sourceLength);
			}
		}

		private readonly string _description;
		private IAliasTable<ICommand<TContext>> _children;

		/// <summary>
		///     The child commands that use a switch token to determine which to execute.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="value" /> is <see langword="null"/>.</exception>
		public IAliasTable<ICommand<TContext>> Children
		{
			get => _children;
			set => _children = value ?? throw new ArgumentNullException(nameof(value));
		}

		/// <summary>
		///     The child command to use when no further tokens are provided.
		///     This command cannot require tokens.
		/// </summary>
		public ICommand<TContext> DefaultChild { get; set; }

		/// <inheritdoc />
		public string Signature
		{
			get
			{
				StringBuilder builder = new StringBuilder();

				char openModeClosure;
				char closeModeClosure;

				if (DefaultChild != null)
				{
					openModeClosure = '(';
					closeModeClosure = ')';
				}
				else
				{
					openModeClosure = '<';
					closeModeClosure = '>';
				}

				if (Children.Any())
				{
					builder.Append("[-h|--help] ");
				}

				builder.Append(openModeClosure);

				builder.Append("mode");
				if (Children.Any())
				{
					builder.Append(": ...");
				}

				builder.Append(closeModeClosure);

				builder.Append(' ');

				builder.Append("[...] <...>");

				return builder.ToString();
			}
		}

		/// <inheritdoc />
		public string Description
		{
			get
			{
				StringBuilder builder = new StringBuilder();

				// Hanging indent to show start of description.
				builder.Append("  ");
				builder.AppendLine(_description);

				if (Children.Any())
				{
					// Page break or continuation of default child command.
					builder.AppendLine();
					builder.Append("  Modes:");

					StringBuilder source = new StringBuilder();

					foreach (KeyValuePair<Aliases, ICommand<TContext>> childCommand in Children)
					{
						RecursiveCommandAppend(childCommand, builder, source);
						source.Clear();
					}
				}

				return builder.ToString();
			}
		}

		/// <summary>
		///     Constructs an instance of <see cref="SwitchCommand{TContext}" />.
		/// </summary>
		/// <param name="description">The description of what the command does.</param>
		/// <exception cref="ArgumentNullException"><paramref name="description" /> is <see langword="null"/>.</exception>
		public SwitchCommand(string description) : this(new AliasTable<ICommand<TContext>>(), description ?? throw new ArgumentNullException(nameof(description)))
		{
		}

		/// <summary>
		///     Constructs an instance of <see cref="SwitchCommand{TContext}" />.
		/// </summary>
		/// <param name="children">The child commands to be used by this switch.</param>
		/// <param name="description">The description of what the command does.</param>
		/// <exception cref="ArgumentNullException"><paramref name="children" /> or <paramref name="description" /> is <see langword="null"/>.</exception>
		public SwitchCommand(IAliasTable<ICommand<TContext>> children, string description)
		{
			_children = children ?? throw new ArgumentNullException(nameof(children));
			_description = description ?? throw new ArgumentNullException(nameof(description));
		}

		/// <inheritdoc />
		/// <exception cref="ArgumentNullException"><paramref name="tokens" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException">A value of <paramref name="tokens"/> is <see langword="null"/>.</exception>
		public virtual string Execute(IEnumerator<string> tokens, TContext data)
		{
			if (tokens == null)
			{
				throw new ArgumentNullException(nameof(tokens));
			}

			// No available tokens
			if (!tokens.MoveNext())
			{
				if (DefaultChild == null)
				{
					throw new CommandException("Switch target was unspecified.");
				}

				// Tokens are always an empty list.
				return DefaultChild.Execute(tokens, data);
			}

			string helpToken = tokens.Current ?? throw new ArgumentException($"A token was null.", nameof(tokens));
			bool isHelp = false;
			if (helpToken == "-h" || helpToken == "--help")
			{
				isHelp = true;

				// Help was the only token
				if (!tokens.MoveNext())
				{
					if (DefaultChild == null)
					{
						throw new CommandException("Help switch target was unspecified.");
					}

					return DefaultChild.GetHelpPage("(default)");
				}
			}

			string commandToken = tokens.Current ?? throw new ArgumentException($"A token was null.", nameof(tokens));
			ICommand<TContext> command = Children[commandToken];
			if (command == null)
			{
				throw new CommandException("Mode was unrecognized.");
			}

			return isHelp ? command.GetHelpPage(commandToken) : command.Execute(tokens, data);
		}
	}
}
