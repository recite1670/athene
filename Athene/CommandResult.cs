﻿using System;

namespace Athene
{
	/// <summary>
	///     Represents the result of the execution of a command.
	/// </summary>
	public readonly struct CommandResult
	{
		/// <summary>
		///     Shortcut for a constructor with <code>NotFound</code> as the outcome parameter.
		/// </summary>
		public static CommandResult NotFound => new CommandResult(ResultOutcome.NotFound, null);

		/// <summary>
		///     Shortcut for a constructor with <code>Fail</code> as the outcome parameter.
		/// </summary>
		/// <param name="message">What to display to the user as a result of executing the command.</param>
		/// <returns>A <see cref="CommandResult" /> with a <code>false</code> <seealso cref="Outcome" /> and a user-defined <seealso cref="Message" />.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="message" /> is <see langword="null"/>.</exception>
		public static CommandResult Fail(string message)
		{
			if (message == null)
			{
				throw new ArgumentNullException(nameof(message));
			}

			return new CommandResult(ResultOutcome.Fail, message);
		}

		/// <summary>
		///     Shortcut for a constructor with <code>Success</code> as the outcome parameter.
		/// </summary>
		/// <param name="message">What to display to the user as a result of executing the command.</param>
		/// <returns>A <see cref="CommandResult" /> with a <code>true</code> <seealso cref="Outcome" /> and a user-defined <seealso cref="Message" />.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="message" /> is <see langword="null"/>.</exception>
		public static CommandResult Succeed(string message)
		{
			if (message == null)
			{
				throw new ArgumentNullException(nameof(message));
			}

			return new CommandResult(ResultOutcome.Success, message);
		}

		/// <summary>
		///     Whether or not the command succeeded.
		/// </summary>
		public ResultOutcome Outcome { get; }
		/// <summary>
		///     What to display to the user as a result of executing the command.
		/// </summary>
		public string Message { get; }

		/// <summary>
		///     Constructs an instance of <see cref="CommandResult" />.
		/// </summary>
		/// <param name="outcome">The result of the command execution.</param>
		/// <param name="message">The message to display to the user.</param>
		public CommandResult(ResultOutcome outcome, string message)
		{
			Outcome = outcome;
			Message = message;
		}
	}
}
