using System.Collections.Generic;

namespace Athene
{
	/// <summary>
	/// 	A data-bound console that can execute console commands with no additional data.
	/// </summary>
	public interface IExecutionContext
	{
		/// <summary>
		///     Safely executes a command.
		/// </summary>
		/// <param name="input">Raw user input.</param>
		/// <returns>A <see cref="CommandResult" /> representing the success and message to show the user.</returns>
		CommandResult Execute(IEnumerable<char> input);
	}
}
