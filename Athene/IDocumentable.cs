﻿namespace Athene
{
	/// <summary>
	///     A self-documents using a short signature and a long description.
	/// </summary>
	public interface IDocumentable
	{
		/// <summary>
		///     Detailed information about the purpose and usage.
		/// </summary>
		string Description { get; }
		/// <summary>
		///     Compact information about the identification or purpose as well as usage.
		/// </summary>
		string Signature { get; }
	}
}
