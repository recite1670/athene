﻿using System.Collections.Generic;

namespace Athene
{
	/// <summary>
	/// 	A type that represents a command.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public interface ICommand<in TContext> : IDocumentable where TContext : IExecutionContext
	{
		/// <summary>
		///     Executes this command.
		/// </summary>
		/// <param name="tokens">All the tokens available to the command.</param>
		/// <param name="context">The current context of the console.</param>
		/// <returns>The text to be shown to the user.</returns>
		string Execute(IEnumerator<string> tokens, TContext context);
	}
}
