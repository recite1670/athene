using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Athene.Tables
{
	/// <summary>
	///     Represents the aliases of an associated value.
	/// </summary>
	public readonly struct Aliases : IEnumerable<string>, IEquatable<Aliases>
	{
		/// <summary>
		///     Performs a value equality check on the two operands.
		/// </summary>
		/// <returns>Whether or not the operands are equal in value.</returns>
		public static bool operator ==(Aliases self, Aliases other)
		{
			if (self.PrimaryAlias != other.PrimaryAlias || self.SecondaryAliases.Count != other.SecondaryAliases.Count)
			{
				return false;
			}

			foreach (string selfSecondaryAlias in self.SecondaryAliases)
			{
				if (!other.SecondaryAliases.Contains(selfSecondaryAlias))
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		///     Performs a value inequality check on the two operands.
		/// </summary>
		/// <returns>Whether or not the operands are unequal in value.</returns>
		public static bool operator !=(Aliases self, Aliases other)
		{
			return !(self == other);
		}

		/// <summary>
		///     The alias the associated value is most commonly referred to.
		/// </summary>
		public string PrimaryAlias { get; }

		/// <summary>
		///     Aliases that the associated value is less commonly known as.
		/// </summary>
		public IReadOnlyList<string> SecondaryAliases { get; }

		/// <summary>
		///     Constructs an instance of <see cref="Aliases" />.
		/// </summary>
		/// <param name="primaryAlias">The alias the command is most commonly referred to.</param>
		/// <param name="secondaryAliases">Aliases that the command is less commonly known as.</param>
		/// <exception cref="ArgumentNullException"><paramref name="primaryAlias" /> or <paramref name="secondaryAliases" /> is <see langword="null" />.</exception>
		/// <exception cref="ArgumentException"><paramref name="secondaryAliases" /> contained a <see langword="null" /> member.</exception>
		public Aliases(string primaryAlias, IEnumerable<string> secondaryAliases)
		{
			if (primaryAlias == null)
			{
				throw new ArgumentNullException(nameof(primaryAlias));
			}

			if (secondaryAliases == null)
			{
				throw new ArgumentNullException(nameof(secondaryAliases));
			}

			string[] secondaryAliasesCopy = secondaryAliases.Select(x => x ?? throw new ArgumentException("A secondary alias was null.", nameof(secondaryAliases)))
				.ToArray();

			PrimaryAlias = primaryAlias;
			SecondaryAliases = secondaryAliasesCopy;
		}

		/// <summary>
		///     Constructs an instance of <see cref="Aliases" />.
		/// </summary>
		/// <param name="primaryAlias">The alias the command is most commonly referred to.</param>
		/// <param name="secondaryAliases">Aliases that the command is less commonly known as.</param>
		/// <exception cref="ArgumentNullException"><paramref name="primaryAlias" /> or <paramref name="secondaryAliases" /> is <see langword="null" />.</exception>
		/// <exception cref="ArgumentException"><paramref name="secondaryAliases" /> contained a <see langword="null" /> member.</exception>
		public Aliases(string primaryAlias, params string[] secondaryAliases)
		{
			if (primaryAlias == null)
			{
				throw new ArgumentNullException(nameof(primaryAlias));
			}

			if (secondaryAliases == null)
			{
				throw new ArgumentNullException(nameof(secondaryAliases));
			}

			for (int i = 0; i < secondaryAliases.Length; ++i)
			{
				if (secondaryAliases[i] == null)
				{
					throw new ArgumentException($"Secondary alias at {i} was null.");
				}
			}

			string[] secondaryAliasesCopy = new string[secondaryAliases.Length];
			Array.Copy(secondaryAliases, secondaryAliasesCopy, secondaryAliases.Length);

			PrimaryAlias = primaryAlias;
			SecondaryAliases = secondaryAliasesCopy;
		}

		/// <inheritdoc cref="IEnumerable{T}.GetEnumerator" />
		public IEnumerator<string> GetEnumerator()
		{
			yield return PrimaryAlias;

			foreach (string alias in SecondaryAliases)
			{
				yield return alias;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		/// <summary>
		///     Performs a value equality check on this and another <see cref="Aliases" />.
		/// </summary>
		/// <param name="other">The other <see cref="Aliases" /> to check the value of.</param>
		/// <returns>Whether or not this object and <paramref name="other" /> are equal.</returns>
		public bool Equals(Aliases other)
		{
			return this == other;
		}

		/// <inheritdoc cref="Object.ToString" />
		public override string ToString()
		{
			return
				SecondaryAliases.Count > 0
					? PrimaryAlias + " (" + string.Join(", ", SecondaryAliases) + ")"
					: PrimaryAlias;
		}

		/// <summary>
		///     Performs a value equality check on this and another object.
		/// </summary>
		/// <param name="obj">The other object to check the value of.</param>
		/// <returns>Whether or not this object and <paramref name="obj" /> are equal.</returns>
		public override bool Equals(object obj)
		{
			return !ReferenceEquals(obj, null) && obj is Aliases aliases && Equals(aliases);
		}

		/// <inheritdoc cref="Object.GetHashCode" />
		public override int GetHashCode()
		{
			unchecked
			{
				return
					((PrimaryAlias != null ? PrimaryAlias.GetHashCode() : 0) * 397) ^
					(SecondaryAliases != null &&
					SecondaryAliases.Count > 0 ? SecondaryAliases.Aggregate(0, (t, i) => t ^ i.GetHashCode()) : 0);
			}
		}
	}
}
