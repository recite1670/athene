using System;
using System.Collections;
using System.Collections.Generic;

namespace Athene.Tables
{
	/// <inheritdoc cref="IAliasTable{TCommand}" />
	public class AliasTable<TValue> : IAliasTable<TValue>
	{
		private readonly Dictionary<Aliases, TValue> _aliasValues;
		private readonly Dictionary<string, TValue> _valueLookup;

		/// <inheritdoc cref="IAliasTable{TValue}.Aliases" />
		public IEnumerable<Aliases> Aliases => _aliasValues.Keys;

		/// <inheritdoc cref="IAliasTable{TValue}.Values" />
		public IEnumerable<TValue> Values => _aliasValues.Values;

		/// <summary>
		///     Constructs an instance of <see cref="AliasTable{TContext}" />
		/// </summary>
		public AliasTable() : this(StringComparer.InvariantCulture)
		{
		}

		/// <summary>
		///     Constructs an instance of <see cref="AliasTable{TContext}" />.
		/// </summary>
		/// <param name="aliasComparer">The comparer to be used when getting/setting the lookup table.</param>
		/// <exception cref="ArgumentNullException"><paramref name="aliasComparer" /> is <see langword="null" />.</exception>
		public AliasTable(IEqualityComparer<string> aliasComparer)
		{
			_aliasValues = new Dictionary<Aliases, TValue>();
			_valueLookup = new Dictionary<string, TValue>(aliasComparer ?? throw new ArgumentNullException(nameof(aliasComparer)));
		}

		/// <inheritdoc cref="IAliasTable{TValue}.this[string]" />
		/// <exception cref="ArgumentNullException"><paramref name="alias" /> is <see langword="null" />.</exception>
		public TValue this[string alias] =>
			_valueLookup.TryGetValue(alias ?? throw new ArgumentNullException(nameof(alias)), out TValue command) ? command : default;

		/// <inheritdoc cref="IAliasTable{TValue}.this[Athene.Tables.Aliases]" />
		/// <exception cref="ArgumentNullException"><paramref name="aliases" /> is <see langword="null" />.</exception>
		public TValue this[Aliases aliases]
		{
			get => _aliasValues.TryGetValue(aliases, out TValue command) ? command : default;
			set
			{
				if (aliases == null)
				{
					throw new ArgumentNullException(nameof(aliases));
				}

				Insert(aliases, value);
			}
		}

		/// <summary>
		///		Adds a value and its associated aliases to the table.
		/// </summary>
		/// <param name="aliases">The aliases of the value.</param>
		/// <param name="value">The value associated with the aliases.</param>
		/// <exception cref="ArgumentNullException"><paramref name="aliases" /> is <see langword="null" />.</exception>
		/// <exception cref="InvalidOperationException">An alias that is to be added is already in use.</exception>
		public void Add(Aliases aliases, TValue value)
		{
			if (aliases == null)
			{
				throw new ArgumentNullException(nameof(aliases));
			}

			foreach (string alias in aliases)
			{
				if (_valueLookup.TryGetValue(alias, out TValue aliasHolder))
				{
					throw new InvalidOperationException($"Alias \"{alias}\" is already in use by command \"{aliasHolder}\".");
				}
			}

			Insert(aliases, value);
		}

		/// <summary>
		///		Removes a value and its associated aliases from the table.
		/// </summary>
		/// <param name="aliases">The aliases to remove, along with the value they represent.</param>
		/// <exception cref="ArgumentNullException"><paramref name="aliases" /> is <see langword="null" />.</exception>
		public bool Remove(Aliases aliases)
		{
			if (aliases == null)
			{
				throw new ArgumentNullException(nameof(aliases));
			}

			_aliasValues.Remove(aliases);

			foreach (string name in aliases)
			{
				_valueLookup.Remove(name);
			}

			return true;
		}

		/// <inheritdoc cref="IEnumerable{T}.GetEnumerator" />
		public IEnumerator<KeyValuePair<Aliases, TValue>> GetEnumerator()
		{
			return _aliasValues.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private void Insert(Aliases aliases, TValue value)
		{
			_aliasValues[aliases] = value;

			foreach (string alias in aliases)
			{
				_valueLookup[alias] = value;
			}
		}
	}
}
