using System.Collections.Generic;

namespace Athene.Tables
{
	/// <summary>
	///     An object for storing and retrieving associated values by alias.
	/// </summary>
	/// <typeparam name="TValue">The value type to be associated with the aliases.</typeparam>
	public interface IAliasTable<TValue> : IEnumerable<KeyValuePair<Aliases, TValue>>
	{
		/// <summary>
		///     The aliases of the table.
		/// </summary>
		IEnumerable<Aliases> Aliases { get; }

		/// <summary>
		///     The values of the table.
		/// </summary>
		IEnumerable<TValue> Values { get; }

		/// <summary>
		///     Gets the value associated with the specified alias.
		/// </summary>
		/// <param name="alias">The alias of the unknown value.</param>
		TValue this[string alias] { get; }

		/// <summary>
		///     Gets the value associated with the specified aliases.
		/// </summary>
		/// <param name="aliases">The aliases of the unknown value.</param>
		TValue this[Aliases aliases] { get; }
	}
}
