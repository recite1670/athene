﻿namespace Athene
{
	/// <summary>
	///     The possible outcomes of executing a command.
	/// </summary>
	public enum ResultOutcome
	{
		/// <summary>
		///     The command was found and executed without failure.
		/// </summary>
		Success,

		/// <summary>
		///     The command was found but failed while executing.
		/// </summary>
		Fail,

		/// <summary>
		///     The command failed because it was not found.
		/// </summary>
		NotFound
	}
}
