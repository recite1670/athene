﻿using System;
using System.Collections.Generic;

namespace Athene.Parameters.Options
{
	/// <summary>
	///     An option whose value represents the raw input of the user.
	/// </summary>
	public sealed class RawValueOption : BaseValueOption<IExecutionContext, string>
	{
		/// <inheritdoc />
		/// <exception cref="ArgumentNullException"><paramref name="description" /> or <paramref name="valueName" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException"><paramref name="shortName" /> and <paramref name="longName" /> are null. Only one may be null.</exception>
		public RawValueOption(char? shortName, string longName, string valueName, string description) : base(shortName, longName,
			valueName ?? throw new ArgumentNullException(nameof(valueName)), description ?? throw new ArgumentNullException(nameof(description)))
		{
		}

		/// <inheritdoc />
		/// <exception cref="ArgumentNullException"><paramref name="tokens" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException">The next value of <paramref name="tokens"/> is <see langword="null"/>.</exception>
		protected override void UpdateValue(IEnumerator<string> tokens, IExecutionContext context)
		{
			if (!tokens.MoveNext())
			{
				throw new CommandException("A value was not provided.");
			}

			Value = tokens.Current ?? throw new ArgumentException($"The value was null.");
		}
	}
}
