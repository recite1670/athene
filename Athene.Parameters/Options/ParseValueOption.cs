﻿using System;
using System.Collections.Generic;

namespace Athene.Parameters.Options
{
	/// <summary>
	///     An option that's value is parsed from the token after it.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	/// <typeparam name="TValue">The type of the parsed option value.</typeparam>
	public class ParseValueOption<TContext, TValue> : BaseValueOption<TContext, TValue> where TContext : IExecutionContext
	{
		private readonly ContextualTryParseHandler<TContext, TValue> _tryParseHandler;

		/// <summary>
		///     Constructs an instance of <see cref="ParseValueOption{TContext, TValue}" />.
		/// </summary>
		/// <param name="shortName">The short, single-character representation of the option.</param>
		/// <param name="longName">The full, multi-character representation of the option.</param>
		/// <param name="valueName">Name of the value related to the option. This could be a type or unit.</param>
		/// <param name="description">Detailed information about the purpose and usage of this instance.</param>
		/// <param name="tryParseHandler">Parse function to convert a token into an instance of <typeparamref name="TValue" />.</param>
		/// <exception cref="ArgumentNullException"><paramref name="description" />, <paramref name="valueName" />, or <paramref name="tryParseHandler" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException"><paramref name="shortName" /> and <paramref name="longName" /> are <see langword="null"/>. Only one may be <see langword="null"/>.</exception>
		public ParseValueOption(char? shortName, string longName, string valueName, string description, ContextualTryParseHandler<TContext, TValue> tryParseHandler) : base(shortName, longName,
			valueName ?? throw new ArgumentNullException(nameof(valueName)), description ?? throw new ArgumentNullException(nameof(description)))
		{
			_tryParseHandler = tryParseHandler ?? throw new ArgumentNullException(nameof(tryParseHandler));
		}

		/// <inheritdoc />
		/// <exception cref="ArgumentNullException"><paramref name="tokens" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException">The next value of <paramref name="tokens"/> is <see langword="null"/>.</exception>
		protected override void UpdateValue(IEnumerator<string> tokens, TContext context)
		{
			if (tokens == null)
			{
				throw new ArgumentNullException(nameof(tokens));
			}

			if (!tokens.MoveNext())
			{
				throw new CommandException("A value was not provided.");
			}

			if (!_tryParseHandler(tokens.Current ?? throw new ArgumentException($"The value was null."), context, out TValue value))
			{
				throw new CommandException("Failed to convert value.");
			}

			Value = value;
		}
	}

	/// <summary>
	///     An option that's value is parsed from the token after it, without using context.
	/// </summary>
	/// <typeparam name="TValue">The type of the parsed option value.</typeparam>
	public sealed class ParseValueOption<TValue> : ParseValueOption<IExecutionContext, TValue>
	{
		/// <summary>
		///     Constructs an instance of <see cref="ParseValueOption{TContext, TValue}" />.
		/// </summary>
		/// <param name="shortName">The short, single-character representation of the option.</param>
		/// <param name="longName">The full, multi-character representation of the option.</param>
		/// <param name="valueName">Name of the value related to the option. This could be a type or unit.</param>
		/// <param name="description">Detailed information about the purpose and usage of this instance.</param>
		/// <param name="tryParseHandler">Parse function to convert a token into an instance of <typeparamref name="TValue" />.</param>
		/// <exception cref="ArgumentNullException"><paramref name="description" />, <paramref name="valueName" />, or <paramref name="tryParseHandler" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException"><paramref name="shortName" /> and <paramref name="longName" /> are <see langword="null"/>. Only one may be <see langword="null"/>.</exception>
		public ParseValueOption(char? shortName, string longName, string valueName, string description, TryParseHandler<TValue> tryParseHandler) : base(shortName, longName,
			valueName, description, (string raw, IExecutionContext _, out TValue result) => tryParseHandler(raw, out result))
		{
		}
	}
}
