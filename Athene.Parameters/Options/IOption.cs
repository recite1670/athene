namespace Athene.Parameters.Options
{
	/// <summary>
	///     Processes an optional token(s) for use in execution of a <see cref="ParameterCommand{TContext}" />.
	/// </summary>
	public interface IOption : IParameter
	{
		/// <summary>
		///     The full, multi-character representation of the option.
		/// </summary>
		string LongName { get; }

		/// <summary>
		///     The short, single-character representation of the option.
		/// </summary>
		char? ShortName { get; }
	}

	/// <inheritdoc cref="IOption"/>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public interface IOption<in TContext> : IOption, IParameter<TContext> where TContext : IExecutionContext
	{
	}
}
