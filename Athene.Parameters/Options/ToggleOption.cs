﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Athene.Parameters.Options
{
	/// <summary>
	///     An option that's value is the mere presence of itself.
	/// </summary>
	public sealed class ToggleOption : BaseOption<IExecutionContext>
	{
		private static string SignatureGenerator(char? shortName, string longName)
		{
			StringBuilder builder = new StringBuilder();

			builder.Append('[');

			if (shortName.HasValue)
			{
				builder.Append('-');
				builder.Append(shortName.Value);
			}

			if (longName != null)
			{
				if (shortName.HasValue)
				{
					builder.Append('|');
				}

				builder.Append("--");
				builder.Append(longName);
			}

			builder.Append(']');

			return builder.ToString();
		}

		/// <inheritdoc />
		/// <exception cref="ArgumentNullException"><paramref name="description" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException"><paramref name="shortName" /> and <paramref name="longName" /> are null. Only one may be null.</exception>
		public ToggleOption(char? shortName, string longName, string description) : base(shortName, longName, description ?? throw new ArgumentNullException(nameof(description)),
			() => SignatureGenerator(shortName, longName))
		{
		}

		/// <inheritdoc />
		protected override void UpdateValue(IEnumerator<string> tokens, IExecutionContext context)
		{
		}
	}
}
