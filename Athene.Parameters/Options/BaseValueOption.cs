﻿using System;
using System.Text;

namespace Athene.Parameters.Options
{
	/// <summary>
	///     An option that represents a value.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	/// <typeparam name="TValue">Type of the value this option represents.</typeparam>
	public abstract class BaseValueOption<TContext, TValue> : BaseOption<TContext>, IValueParameter<TValue> where TContext : IExecutionContext
	{
		private static string SignatureGenerator(char? shortName, string longName, string valueName)
		{
			StringBuilder builder = new StringBuilder();

			builder.Append('[');

			if (shortName.HasValue)
			{
				builder.Append('-');
				builder.Append(shortName.Value);
			}

			if (longName != null)
			{
				if (shortName.HasValue)
				{
					builder.Append('|');
				}

				builder.Append("--");
				builder.Append(longName);
			}

			// Key difference between ToggleOption: equality with value wrapped in angle brackets.
			builder.Append('=');

			builder.Append('<');
			builder.Append(valueName);
			builder.Append('>');

			builder.Append(']');

			return builder.ToString();
		}

		/// <inheritdoc />
		public TValue Value { get; protected set; }

		/// <inheritdoc />
		/// <summary>
		///     Constructs an instance of <see cref="BaseValueOption{TContext, TValue}" />.
		/// </summary>
		/// <param name="shortName">The short, single-character representation of the option.</param>
		/// <param name="longName">The full, multi-character representation of the option.</param>
		/// <param name="valueName">Name of the value related to the option. This could be a type or unit.</param>
		/// <param name="description">Detailed information about the purpose and usage of this instance.</param>
		/// <exception cref="ArgumentNullException"><paramref name="description" /> or <paramref name="valueName" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException"><paramref name="shortName" /> and <paramref name="longName" /> are null. Only one may be null.</exception>
		protected BaseValueOption(char? shortName, string longName, string valueName, string description) : base(shortName, longName, description,
			() => SignatureGenerator(shortName, longName, valueName ?? throw new ArgumentNullException(nameof(valueName))))
		{
		}

		/// <inheritdoc />
		public override void Clear()
		{
			// Discourages using previous execution's value. That information should be stored in the command itself.
			Value = default;

			base.Clear();
		}
	}
}
