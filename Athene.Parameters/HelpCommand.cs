﻿using System;
using Athene.Parameters.Arguments;
using Athene.Tables;

namespace Athene.Parameters
{
	/// <summary>
	///     A command for formatting <see cref="IDocumentable" /> objects in an <see cref="IAliasTable{TValue}"/>.
	/// </summary>
	public class HelpCommand<TContext, TDocumentable> : ParameterCommand<TContext> where TContext : IExecutionContext where TDocumentable : IDocumentable
	{
		private readonly AliasableArgument<TContext, TDocumentable> _aliasable;

		/// <summary>
		///     Constructs an instance of <see cref="HelpCommand{TContext, TDocumentable}" />.
		/// </summary>
		/// <param name="aliasTable">The aliases and associated values that can be used.</param>
		/// <exception cref="ArgumentNullException"><paramref name="aliasTable" /> is <see langword="null"/>.</exception>
		public HelpCommand(IAliasTable<TDocumentable> aliasTable) : base("Displays the documentation of any Athene command.")
		{
			if (aliasTable == null)
			{
				throw new ArgumentNullException(nameof(aliasTable));
			}

			Parameters.Add(_aliasable = new AliasableArgument<TContext, TDocumentable>(aliasTable, "documentable", "alias", "The documentable to show documentation for."));
		}

		/// <inheritdoc cref="ParameterCommand{TContext}.Execute(TContext)" />
		protected override string Execute(TContext context)
		{
			return _aliasable.Value.GetHelpPage(_aliasable.Alias);
		}
	}
}
