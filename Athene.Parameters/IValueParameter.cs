namespace Athene.Parameters
{
	/// <summary>
	/// 	A parameter that represents a typed value.
	/// </summary>
	/// <typeparam name="TValue">The typed value the parameter represents.</typeparam>
	public interface IValueParameter<out TValue> : IParameter
	{
		/// <summary>
		///     Value of the parameter once it is converted from token form.
		/// </summary>
		TValue Value { get; }
	}
}
