﻿using System.Collections.Generic;

namespace Athene.Parameters
{
	/// <summary>
	///     An element that processes, stores, and asserts data for an instance of <see cref="ParameterCommand{TContext}" />.
	/// </summary>
	public interface IParameter : IDocumentable
	{
		/// <summary>
		///     Whether or not this parameter has a value set. This is reset after <seealso cref="Clear()" /> is called.
		/// </summary>
		bool HasValue { get; }
		/// <summary>
		///     Whether or not this parameter must have a value for execution of the command.
		/// </summary>
		bool Required { get; }

		/// <summary>
		///     Resets the value of this parameter. This should also set <seealso cref="HasValue" /> to <code>false</code> upon success.
		/// </summary>
		void Clear();
	}

	/// <summary>
	///     An element that processes, stores, and asserts data for an instance of <see cref="ParameterCommand{TContext}" />.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public interface IParameter<in TContext> : IParameter where TContext : IExecutionContext
	{
		/// <summary>
		///     Sets the value of this parameter. This should also set <see cref="IParameter.HasValue" /> to <code>true</code> upon success.
		/// </summary>
		/// <param name="tokens">Full list of tokens passed to the command.</param>
		/// <param name="context">The external execution data.</param>
		/// <returns>Tokens shifted when updating. 0 signifies no update.</returns>
		bool Update(IEnumerator<string> tokens, TContext context);
	}
}
