﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Athene.Parameters
{
	/// <summary>
	///     A base-command that converts the information provided by the user from token representation into object representation.
	/// </summary>
	/// <typeparam name="TContext">The type of the data to be passed for executing commands.</typeparam>
	public abstract class ParameterCommand<TContext> : ICommand<TContext> where TContext : IExecutionContext
	{
		private readonly string _description;

		/// <summary>
		///     A full list of parameters managed.
		/// </summary>
		protected List<IParameter<TContext>> Parameters { get; }

		/// <inheritdoc cref="IDocumentable.Signature" />
		public string Signature => string.Join(" ", Parameters.Select(x => x.Signature));

		/// <inheritdoc cref="IDocumentable.Description" />
		public string Description
		{
			get
			{
				StringBuilder builder = new StringBuilder();

				// Hanging indent to show start of description.
				builder.Append("  ");
				builder.Append(_description);

				if (Parameters.Count > 0)
				{
					// Page break.
					builder.AppendLine();

					string[] signatures = new string[Parameters.Count];
					for (int i = 0; i < Parameters.Count; ++i)
					{
						signatures[i] = Parameters[i].Signature;
					}

					// Used for aligning the definition of parameters.
					int maxSignatureLength = signatures.Max(x => x.Length);

					for (int i = 0; i < Parameters.Count; ++i)
					{
						IParameter<TContext> parameter = Parameters[i];

						builder.AppendLine();

						string signature = signatures[i];

						// Hanging indent.
						builder.Append("  ");
						builder.Append(signature);

						// Padding.
						// + 1 is for at least 1 space between the signature end and the hyphen.
						builder.Append(' ', maxSignatureLength - signature.Length + 1);
						builder.Append('-');
						builder.Append(' ');

						// Description.
						builder.Append(parameter.Description);
					}
				}

				return builder.ToString();
			}
		}

		/// <summary>
		///     Constructs an instance of <see cref="ParameterCommand{TContext}" />.
		/// </summary>
		/// <param name="description">A detailed purpose of this command.</param>
		protected ParameterCommand(string description)
		{
			_description = description;

			Parameters = new List<IParameter<TContext>>();
		}

		/// <inheritdoc cref="ICommand{TContext}.Execute" />
		/// <exception cref="ArgumentNullException"><paramref name="tokens" /> is <see langword="null"/>.</exception>
		public string Execute(IEnumerator<string> tokens, TContext context)
		{
			if (tokens == null)
			{
				throw new ArgumentNullException(nameof(tokens));
			}

			// Clear all parameters to make sure options from last execution don't stay.
			foreach (IParameter<TContext> parameter in Parameters)
			{
				parameter.Clear();
			}

			while (tokens.MoveNext())
			{
				// True if any parameter matched with the current token.
				bool matchedAny = false;

				foreach (IParameter<TContext> parameter in Parameters)
				{
					// Only run if the parameter does not have value, otherwise options with short and long names could have duplicate execution during the same command execution.
					if (parameter.HasValue)
					{
						continue;
					}

					try
					{
						// Update the parameter
						matchedAny = parameter.Update(tokens, context);
					}
					catch (CommandException e)
					{
						// Nest exception message in new exception with parameter data and throw.
						throw new CommandException($"Invalid parameter for {parameter.Signature}: {e.Message}");
					}
					catch (Exception e)
					{
						// Nest exception object in new exception with parameter data and throw.
						throw new AggregateException($"Unhandled exception when updating {parameter.Signature}.", e);
					}

					if (matchedAny)
					{
						// Break in order to start a fresh iteration.
						break;
					}
				}

				// See declaration above.
				if (!matchedAny)
				{
					// tokenI will be the original index because tokenI is only mutated when matchedAny is true, which is impossible.
					throw new CommandException($"Unexpected token.");
				}
			}

			// Make sure all parameters are set before calling the safe execution method.
			foreach (IParameter<TContext> parameter in Parameters)
			{
				if (parameter.Required && !parameter.HasValue)
				{
					throw new CommandException($"The required parameter of {parameter.Signature} was not fulfilled.");
				}
			}

			return Execute(context);
		}

		/// <summary>
		///     Executes the command after all parameters have been cleared and read from the token list.
		/// </summary>
		/// <returns>Information to show the user.</returns>
		protected abstract string Execute(TContext context);
	}
}
