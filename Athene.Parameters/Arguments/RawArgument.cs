﻿using System;

namespace Athene.Parameters.Arguments
{
	/// <summary>
	///     An argument that represents the raw input of the user.
	/// </summary>
	public sealed class RawArgument : BaseArgument<IExecutionContext, string>
	{
		/// <summary>
		///     Constructs an instance of <see cref="RawArgument" />.
		/// </summary>
		/// <param name="name">Name of this argument.</param>
		/// <param name="valueName">Name of this argument's value.</param>
		/// <param name="description">Detailed purpose of this argument.</param>
		/// <exception cref="ArgumentNullException"><paramref name="name" />, <paramref name="valueName" />, or <paramref name="description" /> is <see langword="null"/>.</exception>
		public RawArgument(string name, string valueName, string description) : base(name, valueName, description)
		{
		}

		/// <inheritdoc />
		protected override string ProcessValue(string token, IExecutionContext context)
		{
			return token;
		}
	}
}
