﻿using System;
using System.Collections.Generic;

namespace Athene.Parameters.Arguments
{
	/// <summary>
	///     Processes a mandatory token(s) for use in execution of a <see cref="ParameterCommand{TContext}" />.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	/// <typeparam name="TValue">The typed value that the argument represents.</typeparam>
	public abstract class BaseArgument<TContext, TValue> : IParameter<TContext>, IValueParameter<TValue> where TContext : IExecutionContext
	{
		/// <inheritdoc />
		public TValue Value { get; private set; }

		/// <inheritdoc />
		public string Signature { get; }

		/// <inheritdoc />
		public string Description { get; }

		/// <inheritdoc />
		public bool Required { get; } = true;

		/// <inheritdoc />
		public bool HasValue { get; private set; }

		/// <summary>
		///     Constructs an instance of <see cref="BaseArgument{TContext, TValue}" />.
		/// </summary>
		/// <param name="name">Name of this argument.</param>
		/// <param name="valueName">Name of this argument's value.</param>
		/// <param name="description">Detailed purpose of this argument.</param>
		/// <exception cref="ArgumentNullException"><paramref name="name" />, <paramref name="valueName" />, or <paramref name="description" /> is <see langword="null"/>.</exception>
		protected BaseArgument(string name, string valueName, string description)
		{
			if (name == null)
			{
				throw new ArgumentNullException(nameof(name));
			}

			if (valueName == null)
			{
				throw new ArgumentNullException(nameof(valueName));
			}

			if (description == null)
			{
				throw new ArgumentNullException(nameof(valueName));
			}

			Signature = $"<{name}: {valueName}>";
			Description = description;
		}

		/// <inheritdoc />
		/// <exception cref="ArgumentNullException"><paramref name="tokens" /> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException">A value of<paramref name="tokens"/> is <see langword="null"/>.</exception>
		public bool Update(IEnumerator<string> tokens, TContext context)
		{
			if (tokens == null)
			{
				throw new ArgumentNullException(nameof(tokens));
			}

			// Process value and advance to next token.
			Value = ProcessValue(tokens.Current ?? throw new ArgumentException($"Token was null.", nameof(tokens)), context);
			HasValue = true;

			return true;
		}

		/// <inheritdoc />
		public virtual void Clear()
		{
			HasValue = false;
		}

		/// <summary>
		///     Processed the raw, <see langword="string" /> representation of the value into typed form.
		/// </summary>
		/// <param name="token">The raw user input.</param>
		/// <param name="context">The currently running execution context.</param>
		/// <returns>The typed form of the user input.</returns>
		protected abstract TValue ProcessValue(string token, TContext context);

		/// <inheritdoc />
		public override string ToString()
		{
			return Signature;
		}
	}
}
