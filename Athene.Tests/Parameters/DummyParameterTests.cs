using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Athene.Tests.Parameters
{
	public class DummyParameterTests
	{
		[Fact]
		public void Description_Clean_NotNull()
		{
			Assert.NotNull(new DummyParameter().Description);
		}

		[Fact]
		public void Signature_Clean_NotNull()
		{
			Assert.NotNull(new DummyParameter().Signature);
		}

		[Fact]
		[SuppressMessage("ReSharper", "UnusedVariable")]
		public void HasValue_Clean_Pass()
		{
			bool temp = new DummyParameter().HasValue;
		}

		[Fact]
		[SuppressMessage("ReSharper", "UnusedVariable")]
		public void Required_Clean_Pass()
		{
			bool temp = new DummyParameter().Required;
		}

		[Fact]
		public void Update_Clean_NotImplementedException()
		{
			DummyParameter parameter = new DummyParameter();

			Assert.Throws<NotImplementedException>(() => parameter.Update(null, null));
		}

		[Fact]
		public void Clear_Clean_NotImplementedException()
		{
			DummyParameter parameter = new DummyParameter();

			Assert.Throws<NotImplementedException>(() => parameter.Clear());
		}
	}
}
