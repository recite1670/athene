using Athene.Parameters;
using Athene.Parameters.Arguments;
using Athene.Parameters.Options;

namespace Athene.Tests.Parameters
{
	public class TestableParameterCommand : ParameterCommand<IExecutionContext>
	{
		private readonly ToggleOption _toggleOption;
		private readonly ParseValueOption<int> _parseOption;
		private readonly ParseArgument<int> _parseArgument;

		public TestableParameterCommand() : base("Example.")
		{
			Parameters.Add(_toggleOption = new ToggleOption('t', "toggle", "Example " + nameof(ToggleOption) + "."));
			Parameters.Add(_parseOption = new ParseValueOption<int>(null, "parse-option", "int", "Example " + nameof(ParseValueOption<int>) + ".", int.TryParse));
			Parameters.Add(_parseArgument = new ParseArgument<int>("parse-argument", "int", "Example " + nameof(ParseArgument<int>) + ".", int.TryParse));
		}

		protected override string Execute(IExecutionContext context)
		{
			return $"{_toggleOption}: {_toggleOption.HasValue} | {_parseOption} {_parseOption.HasValue} {_parseOption.Value} | {_parseArgument} {_parseArgument.HasValue} {_parseArgument.Value}";
		}
	}
}
