using System;
using System.Collections.Generic;
using System.Linq;
using Athene.Parameters.Options;
using Xunit;

namespace Athene.Tests.Parameters
{
	public class ParseValueOptionTests
	{
		[Fact]
		public void Update_NoToken_CommandException()
		{
			ParseValueOption<int> option = new ParseValueOption<int>('p', "parse", "int", "Example option.", int.TryParse);
			IEnumerator<string> enumerator = new[]
			{
				"-p"
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<CommandException>(() => option.Update(enumerator, null));
		}

		[Fact]
		public void Update_NullToken_ArgumentException()
		{
			ParseValueOption<int> option = new ParseValueOption<int>('p', "parse", "int", "Example option.", int.TryParse);
			IEnumerator<string> enumerator = new[]
			{
				"-p",
				null
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<ArgumentException>(() => option.Update(enumerator, null));
		}

		[Fact]
		public void Update_BadToken_CommandException()
		{
			ParseValueOption<int> option = new ParseValueOption<int>('p', "parse", "int", "Example option.", int.TryParse);
			IEnumerator<string> enumerator = new[]
			{
				"-p",
				"This is not a number"
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<CommandException>(() => option.Update(enumerator, null));
		}
	}
}
