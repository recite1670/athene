using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Parameters.Arguments;
using Athene.Tables;
using Athene.Tests.Commands;
using Xunit;

namespace Athene.Tests.Parameters
{
	public class CommandArgumentTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			}, "example", "command", "Example argument.");
		}

		[Fact]
		public void Constructor_NullCommands_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new AliasableArgument<IDocumentable>(null, "example", "command", "Example argument."));
		}

		[Fact]
		public void Constructor_NullName_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			}, null, "command", "Example argument."));
		}

		[Fact]
		public void Constructor_NullValueName_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			}, "example", null, "Example argument."));
		}

		[Fact]
		public void Constructor_NullDescription_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			}, "example", "command", null));
		}

		[Fact]
		public void Clear_Pass()
		{
			AliasableArgument<IDocumentable> argument = new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			}, "example", "command", "Example argument.");

			argument.Clear();
		}

		[Fact]
		public void ProcessValue_OneCommand_Equal()
		{
			DummyCommand command = new DummyCommand();
			AliasableArgument<IDocumentable> argument = new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = command
			}, "example", "command", "Example argument.");
			IEnumerator<string> enumerator = new[]
			{
				nameof(DummyCommand)
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.True(argument.Update(enumerator, null));
			Assert.Equal(command, argument.Value);
		}

		[Fact]
		public void ProcessValue_BadCommand_CommandException()
		{
			AliasableArgument<IDocumentable> argument = new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			}, "example", "command", "Example argument.");
			IEnumerator<string> enumerator = new[]
			{
				nameof(DummyCommand).GetHashCode().ToString()
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<CommandException>(() => argument.Update(enumerator, null));
		}

		[Fact]
		public void ProcessValue_NullTokens_ArgumentNullException()
		{
			AliasableArgument<IDocumentable> argument = new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			}, "example", "command", "Example argument.");

			Assert.Throws<ArgumentNullException>(() => argument.Update(null, null));
		}

		[Fact]
		public void ProcessValue_NullToken_ArgumentException()
		{
			AliasableArgument<IDocumentable> argument = new AliasableArgument<IDocumentable>(new AliasTable<IDocumentable>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			}, "example", "command", "Example argument.");
			IEnumerator<string> enumerator = new string[]
			{
				null
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<ArgumentException>(() => argument.Update(enumerator, null));
		}
	}
}
