using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Parameters.Options;
using Xunit;

namespace Athene.Tests.Parameters
{
	public class RawValueOptionTests
	{
		private const string Value = "ExampleValue";

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new RawValueOption('e', "example", "raw", "Example option.");
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor_NullShortName()
		{
			new RawValueOption(null, "example", "raw", "Example option.");
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor_NullLongName()
		{
			new RawValueOption('e', null, "raw", "Example option.");
		}

		[Fact]
		public void Constructor_NullNames_ArgumentNullException()
		{
			Assert.Throws<ArgumentException>(() => new RawValueOption(null, null, "raw", "Example option."));
		}

		[Fact]
		public void Constructor_NullValueName_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new RawValueOption('e', "example", null, "Example option."));
		}

		[Fact]
		public void Constructor_NullDescription_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new RawValueOption('e', "example", "raw", null));
		}

		[Fact]
		public void ProcessValue_ShortName_Equal()
		{
			RawValueOption argument = new RawValueOption('e', "example", "raw", "Example option.");
			IEnumerator<string> enumerator = new[]
			{
				"-e",
				Value
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.False(argument.HasValue);
			Assert.True(argument.Update(enumerator, null));
			Assert.True(argument.HasValue);
			Assert.Equal(Value, argument.Value);
		}

		[Fact]
		public void ProcessValue_LongName_Equal()
		{
			RawValueOption argument = new RawValueOption('e', "example", "raw", "Example option.");
			IEnumerator<string> enumerator = new[]
			{
				"--example",
				Value
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.False(argument.HasValue);
			Assert.True(argument.Update(enumerator, null));
			Assert.True(argument.HasValue);
			Assert.Equal(Value, argument.Value);
		}

		[Fact]
		public void ProcessValue_ShortNameOnly_Pass()
		{
			RawValueOption argument = new RawValueOption('e', "example", "raw", "Example option.");
			IEnumerator<string> enumerator = new[]
			{
				"-e"
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<CommandException>(() => argument.Update(enumerator, null));
		}

		[Fact]
		public void ProcessValue_LongNameOnly_Pass()
		{
			RawValueOption argument = new RawValueOption('e', "example", "raw", "Example option.");
			IEnumerator<string> enumerator = new[]
			{
				"--example"
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<CommandException>(() => argument.Update(enumerator, null));
		}

		[Fact]
		public void ProcessValue_NullTokens_ArgumentNullException()
		{
			RawValueOption argument = new RawValueOption('e', "example", "raw", "Example option.");

			Assert.Throws<ArgumentNullException>(() => argument.Update(null, null));
		}

		[Fact]
		public void ProcessValue_NullToken_ArgumentException()
		{
			RawValueOption argument = new RawValueOption('e', "example", "raw", "Example option.");
			IEnumerator<string> enumerator = new string[]
			{
				null
			}.AsEnumerable().GetEnumerator();

			enumerator.MoveNext();

			Assert.Throws<ArgumentException>(() => argument.Update(enumerator, null));
		}

		[Fact]
		public void ProcessValue_IndexOutOfRange_ArgumentOutOfRangeException()
		{
			RawValueOption argument = new RawValueOption('e', "example", "raw", "Example option.");
			string[] tokens =
			{
				"--example"
			};
			IEnumerator<string> enumerator = tokens.AsEnumerable().GetEnumerator();

			for (int i = 0; i < tokens.Length; ++i)
			{
				enumerator.MoveNext();
			}

			Assert.Throws<CommandException>(() => argument.Update(enumerator, null));
		}
	}
}
