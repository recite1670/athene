using System;
using System.Collections.Generic;
using Athene.Parameters;

namespace Athene.Tests.Parameters
{
	public class DummyParameter : IParameter<IExecutionContext>
	{
		public string Description { get; } = "Description here.";

		public string Signature { get; } = "Signautre here.";

		public bool HasValue { get; } = false;

		public bool Required { get; } = true;

		public bool Update(IEnumerator<string> tokens, IExecutionContext context)
		{
			throw new NotImplementedException();
		}

		public void Clear()
		{
			throw new NotImplementedException();
		}
	}
}
