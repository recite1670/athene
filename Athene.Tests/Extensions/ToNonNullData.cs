using System.Collections;
using System.Collections.Generic;

namespace Athene.Tests.Extensions
{
	public class ToNonNullData : IEnumerable<object[]>
	{
		public IEnumerator<object[]> GetEnumerator()
		{
			yield return new object[] { new[] { "a", "b", "c", "d" } };
			yield return new object[] { new[] { "abc", "bcd", "cde", "def" } };
			yield return new object[] { new string[0] };
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
