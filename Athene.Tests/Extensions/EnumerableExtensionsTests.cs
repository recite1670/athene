using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Extensions;
using Xunit;

namespace Athene.Tests.Extensions
{
	[SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
	public class EnumerableExtensionsTests
	{
		[Theory]
		[ClassData(typeof(ToNonNullData))]
		public void ToNonNull(string[] array)
		{
			IEnumerable nonNullEnumerable = EnumerableExtensions.ToNonNull((IEnumerable) array);

			Assert.NotNull(nonNullEnumerable);
			Assert.Equal(array.AsEnumerable(), nonNullEnumerable.Cast<string>());
		}

		[Theory]
		[ClassData(typeof(ToNonNullData))]
		public void ToNonNullGeneric(string[] array)
		{
			IEnumerable<string> nonNullEnumerable = EnumerableExtensions.ToNonNull(array);

			Assert.NotNull(nonNullEnumerable);
			Assert.Equal(array.AsEnumerable(), nonNullEnumerable);
		}
	}
}
