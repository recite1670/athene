using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Extensions;
using Xunit;

namespace Athene.Tests.Extensions
{
	[SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
	public class EnumeratorExtensionsTests
	{
		[Theory]
		[ClassData(typeof(ToNonNullData))]
		public void ToNonNull(string[] array)
		{
			IEnumerator arrayEnumerator = array.GetEnumerator();
			IEnumerator nonNullEnumerator = EnumeratorExtensions.ToNonNull(array.GetEnumerator());

			Assert.NotNull(nonNullEnumerator);

			while (true)
			{
				bool arrayMoved = arrayEnumerator.MoveNext();
				bool nonNullMoved = nonNullEnumerator.MoveNext();

				Assert.Equal(arrayMoved, nonNullMoved);

				if (!arrayMoved)
				{
					break;
				}

				Assert.Equal(arrayEnumerator.Current, nonNullEnumerator.Current);
			}
		}

		[Theory]
		[ClassData(typeof(ToNonNullData))]
		public void ToNonNullGeneric(string[] array)
		{
			IEnumerator<string> arrayEnumerator = array.AsEnumerable().GetEnumerator();
			IEnumerator<string> nonNullEnumerator = EnumeratorExtensions.ToNonNull(array.AsEnumerable().GetEnumerator());

			Assert.NotNull(nonNullEnumerator);

			while (true)
			{
				bool arrayMoved = arrayEnumerator.MoveNext();
				bool nonNullMoved = nonNullEnumerator.MoveNext();

				Assert.Equal(arrayMoved, nonNullMoved);

				if (!arrayMoved)
				{
					break;
				}

				Assert.Equal(arrayEnumerator.Current, nonNullEnumerator.Current);
			}
		}

		[Theory]
		[InlineData(new[] { 0, 1, 2, 3 }, 0)]
		[InlineData(new[] { 123, 456, 789, 012, 345, 678, 901 }, 123)]
		public void TakeNext(int[] raw, int expected)
		{
			using (IEnumerator<int> rawEnumerator = raw.AsEnumerable().GetEnumerator())
			{
				Assert.Equal(expected, EnumeratorExtensions.TakeNext(rawEnumerator));
			}
		}

		[Fact]
		public void TakeNext_Null_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => EnumeratorExtensions.TakeNext((IEnumerator<int>) null));
		}

		[Fact]
		public void TakeNext_Empty_ArgumentException()
		{
			using (IEnumerator<int> rawEnumerator = new int[0].AsEnumerable().GetEnumerator())
			{
				Assert.Throws<ArgumentException>(() => EnumeratorExtensions.TakeNext(rawEnumerator));
			}
		}

		[Fact]
		public void TryTakeNext_Null_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => EnumeratorExtensions.TryTakeNext((IEnumerator<int>) null, out _));
		}

		[Fact]
		public void TryTakeNext_Empty_FalseAndDefault()
		{
			using (IEnumerator<int> rawEnumerator = new int[0].AsEnumerable().GetEnumerator())
			{
				Assert.False(EnumeratorExtensions.TryTakeNext(rawEnumerator, out int value));
				Assert.Equal(default, value);
			}
		}

		[Theory]
		[InlineData(new[] { 0, 1, 2, 3 }, 2, new[] { 0, 1 })]
		[InlineData(new[] { 123, 456, 789, 012, 345, 678, 901 }, 4, new[] { 123, 456, 789, 012 })]
		public void Take(int[] raw, int count, int[] expected)
		{
			using (IEnumerator<int> rawEnumerator = raw.AsEnumerable().GetEnumerator())
			{
				Assert.Equal(expected, EnumeratorExtensions.Take(rawEnumerator, count));
			}
		}

		[Theory]
		[InlineData(0)]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(int.MaxValue)]
		public void Take_Null_ArgumentNullException(int count)
		{
			IEnumerable<int> result = EnumeratorExtensions.Take((IEnumerator<int>) null, count);

			Assert.Throws<ArgumentNullException>(() => result.ToList());
		}

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(int.MaxValue)]
		public void Take_Empty_ArgumentException(int count)
		{
			using (IEnumerator<int> rawEnumerator = new int[0].AsEnumerable().GetEnumerator())
			{
				IEnumerable<int> result = EnumeratorExtensions.Take(rawEnumerator, count);

				Assert.Throws<ArgumentException>(() => result.ToList());
			}
		}

		[Theory]
		[InlineData(-1)]
		[InlineData(-2)]
		[InlineData(int.MinValue)]
		public void Take_NegativeCount_ArgumentOutOfRangeException(int count)
		{
			using (IEnumerator<int> rawEnumerator = new int[0].AsEnumerable().GetEnumerator())
			{
				IEnumerable<int> result = EnumeratorExtensions.Take(rawEnumerator, count);

				Assert.Throws<ArgumentOutOfRangeException>(() => result.ToList());
			}
		}

		[Theory]
		[InlineData(new[] { 0, 1, 2, 3 }, 2, new[] { 0, 1 })]
		[InlineData(new[] { 123, 456, 789, 012, 345, 678, 901 }, 4, new[] { 123, 456, 789, 012 })]
		public void TakeToArray(int[] raw, int count, int[] expected)
		{
			using (IEnumerator<int> rawEnumerator = raw.AsEnumerable().GetEnumerator())
			{
				Assert.Equal(expected, EnumeratorExtensions.TakeToArray(rawEnumerator, count));
			}
		}

		[Theory]
		[InlineData(0)]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(int.MaxValue)]
		public void TakeToArray_Null_ArgumentNullException(int count)
		{
			Assert.Throws<ArgumentNullException>(() => EnumeratorExtensions.TakeToArray((IEnumerator<int>) null, count));
		}

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(100)] // Don't use int.MaxValue, it will throw because it cant alloc an array with 2.1 billion members
		public void TakeToArray_Empty_ArgumentException(int count)
		{
			using (IEnumerator<int> rawEnumerator = new int[0].AsEnumerable().GetEnumerator())
			{
				Assert.Throws<ArgumentException>(() => EnumeratorExtensions.TakeToArray(rawEnumerator, count));
			}
		}

		[Theory]
		[InlineData(-1)]
		[InlineData(-2)]
		[InlineData(int.MinValue)]
		public void TakeToArray_NegativeCount_ArgumentOutOfRangeException(int count)
		{
			using (IEnumerator<int> rawEnumerator = new int[0].AsEnumerable().GetEnumerator())
			{
				Assert.Throws<ArgumentOutOfRangeException>(() => EnumeratorExtensions.TakeToArray(rawEnumerator, count));
			}
		}

		[Theory]
		[InlineData(0)]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(int.MaxValue)]
		public void TryTakeToArray_Null_ArgumentNullException(int count)
		{
			Assert.Throws<ArgumentNullException>(() => EnumeratorExtensions.TryTakeToArray((IEnumerator<int>) null, count, out _));
		}

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(100)] // Don't use int.MaxValue, it will throw because it cant alloc an array with 2.1 billion members
		public void TryTakeToArray_Empty_FalseAndNull(int count)
		{
			using (IEnumerator<int> rawEnumerator = new int[0].AsEnumerable().GetEnumerator())
			{
				Assert.False(EnumeratorExtensions.TryTakeToArray(rawEnumerator, count, out int[] values));
				Assert.Null(values);
			}
		}

		[Theory]
		[InlineData(-1)]
		[InlineData(-2)]
		[InlineData(int.MinValue)]
		public void TryTakeToArray_NegativeCount_ArgumentOutOfRangeException(int count)
		{
			using (IEnumerator<int> rawEnumerator = new int[0].AsEnumerable().GetEnumerator())
			{
				Assert.Throws<ArgumentOutOfRangeException>(() => EnumeratorExtensions.TryTakeToArray(rawEnumerator, count, out _));
			}
		}
	}
}
