using System.Collections;
using Xunit;

namespace Athene.Tests.Extensions
{
	public class ToNonNullDataTests
	{
		[Fact]
		public void GenericGetEnumerator()
		{
			Assert.NotNull(new ToNonNullData().GetEnumerator());
		}

		[Fact]
		public void GetEnumerator()
		{
			Assert.NotNull(((IEnumerable) new ToNonNullData()).GetEnumerator());
		}
	}
}
