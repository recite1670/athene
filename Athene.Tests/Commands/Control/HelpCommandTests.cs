using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Parameters;
using Athene.Tables;
using Xunit;

namespace Athene.Tests.Commands.Control
{
	public class HelpCommandTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new HelpCommand<IExecutionContext, ICommand<IExecutionContext>>(new AliasTable<ICommand<IExecutionContext>>());
		}

		[Fact]
		public void Constructor_NullCommands_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new HelpCommand<IExecutionContext, ICommand<IExecutionContext>>(null));
		}

		[Fact]
		public void FormatHelp_Clean_ArgumentNullException()
		{
			Assert.NotNull(new DummyCommand().GetHelpPage(nameof(DummyCommand)));
		}

		[Fact]
		public void FormatHelp_NullAlias_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new DummyCommand().GetHelpPage(null));
		}

		[Fact]
		public void FormatHelp_NullCommand_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => ((ICommand<IExecutionContext>) null).GetHelpPage(nameof(DummyCommand)));
		}

		[Fact]
		public void Execute_OneCommand_NotNull()
		{
			HelpCommand<IExecutionContext, ICommand<IExecutionContext>> command = new HelpCommand<IExecutionContext, ICommand<IExecutionContext>>(new AliasTable<ICommand<IExecutionContext>>
			{
				[new Aliases(nameof(DummyCommand))] = new DummyCommand()
			});

			Assert.NotNull(command.Execute(new[] { nameof(DummyCommand) }.AsEnumerable().GetEnumerator(), null));
		}

		[Fact]
		public void Execute_NullTokens_ArgumentNullException()
		{
			HelpCommand<IExecutionContext, ICommand<IExecutionContext>> command = new HelpCommand<IExecutionContext, ICommand<IExecutionContext>>(new AliasTable<ICommand<IExecutionContext>>());

			Assert.Throws<ArgumentNullException>(() => command.Execute(null, null));
		}
	}
}
