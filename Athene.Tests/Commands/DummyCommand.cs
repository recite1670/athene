using System;
using System.Collections.Generic;

namespace Athene.Tests.Commands
{
	public class DummyCommand : ICommand<IExecutionContext>
	{
		public string Description { get; } = "Description here.";

		public string Signature { get; } = "Signature here.";

		public string Execute(IEnumerator<string> tokens, IExecutionContext context)
		{
			throw new NotImplementedException();
		}
	}
}
