using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Athene.Tests.Commands
{
	public class DummyCommandTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new DummyCommand();
		}

		[Fact]
		public void DescriptionGetter_Clean_NotNull()
		{
			DummyCommand command = new DummyCommand();

			Assert.NotNull(command.Description);
		}

		[Fact]
		public void SignatureGetter_Clean_NotNull()
		{
			DummyCommand command = new DummyCommand();

			Assert.NotNull(command.Signature);
		}

		[Fact]
		public void Execute_Clean_NotImplementedException()
		{
			DummyCommand command = new DummyCommand();

			Assert.Throws<NotImplementedException>(() => command.Execute(null, null));
		}
	}
}
