using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Athene.Tests.Commands
{
	public class FooBarCommandTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new FooBarCommand();
		}

		[Fact]
		public void DescriptionGetter_Clean_NotNull()
		{
			FooBarCommand command = new FooBarCommand();

			Assert.NotNull(command.Description);
		}

		[Fact]
		public void SignatureGetter_Clean_NotNull()
		{
			FooBarCommand command = new FooBarCommand();

			Assert.NotNull(command.Signature);
		}

		[Fact]
		public void Execute_Clean_Equal()
		{
			FooBarCommand command = new FooBarCommand();

			Assert.Equal(FooBarCommand.Output, command.Execute(null, null));
		}
	}
}
