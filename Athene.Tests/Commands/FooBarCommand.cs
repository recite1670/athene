using System.Collections.Generic;

namespace Athene.Tests.Commands
{
	public class FooBarCommand : ICommand<IExecutionContext>
	{
		public const string Output = "Foo bar!";

		public string Description { get; } = "Description here.";

		public string Signature { get; } = "Signature here.";

		public string Execute(IEnumerator<string> tokens, IExecutionContext context)
		{
			return Output;
		}
	}
}
