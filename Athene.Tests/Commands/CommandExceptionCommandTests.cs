using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Athene.Tests.Commands
{
	public class CommandExceptionCommandTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new CommandExceptionCommand();
		}

		[Fact]
		public void DescriptionGetter_Clean_NotNull()
		{
			CommandExceptionCommand command = new CommandExceptionCommand();

			Assert.NotNull(command.Description);
		}

		[Fact]
		public void SignatureGetter_Clean_NotNull()
		{
			CommandExceptionCommand command = new CommandExceptionCommand();

			Assert.NotNull(command.Signature);
		}

		[Fact]
		public void Execute_Clean_NotImplementedException()
		{
			CommandExceptionCommand command = new CommandExceptionCommand();

			Assert.Throws<CommandException>(() => command.Execute(null, null));
		}
	}
}
