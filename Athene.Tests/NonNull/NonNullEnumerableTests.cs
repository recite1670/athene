using System;
using System.Diagnostics.CodeAnalysis;
using Athene.Extensions;
using Xunit;

namespace Athene.Tests.NonNull
{
	[SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
	public class NonNullEnumerableTests
	{
		[Fact]
		public void ToNonNull()
		{
			EnumerableExtensions.ToNonNull(new SuccessEnumerable(null));
		}

		[Fact]
		public void ToNonNull_NullEnumerable_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.ToNonNull(null));
		}

		[Fact]
		public void GetEnumerator()
		{
			Assert.NotNull(EnumerableExtensions.ToNonNull(new SuccessEnumerable(null)).GetEnumerator());
		}
	}
}
