using System.Collections;
using System.Collections.Generic;

namespace Athene.Tests.NonNull
{
	public class SuccessEnumerable : IEnumerable
	{
		private readonly object _value;

		public SuccessEnumerable(object value)
		{
			_value = value;
		}

		public IEnumerator GetEnumerator()
		{
			return new SuccessEnumerator(_value);
		}
	}

	public class SuccessEnumerable<TValue> : IEnumerable<TValue>
	{
		private readonly TValue _value;

		public SuccessEnumerable(TValue value)
		{
			_value = value;
		}

		public IEnumerator<TValue> GetEnumerator()
		{
			return new SuccessEnumerator<TValue>(_value);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
