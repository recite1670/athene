using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using Athene.Extensions;
using Xunit;

namespace Athene.Tests.NonNull
{
	[SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
	public class GenericNonNullEnumerableTests
	{
		[Fact]
		public void ToNonNull()
		{
			EnumerableExtensions.ToNonNull(new SuccessEnumerable<object>(null));
		}

		[Fact]
		public void ToNonNull_NullEnumerable_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => EnumerableExtensions.ToNonNull<object>(null));
		}

		[Fact]
		public void GetEnumeratorOfType_NonNull()
		{
			Assert.NotNull(EnumerableExtensions.ToNonNull(new SuccessEnumerable<object>(null)).GetEnumerator());
		}

		[Fact]
		public void GetEnumerator_NonNull()
		{
			Assert.NotNull(((IEnumerable) EnumerableExtensions.ToNonNull(new SuccessEnumerable<object>(null))).GetEnumerator());
		}
	}
}
