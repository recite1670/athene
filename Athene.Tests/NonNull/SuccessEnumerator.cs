using System.Collections;
using System.Collections.Generic;

namespace Athene.Tests.NonNull
{
	public class SuccessEnumerator : IEnumerator
	{
		public object Current { get; }

		public SuccessEnumerator(object value)
		{
			Current = value;
		}

		public bool MoveNext()
		{
			throw new SuccessException();
		}

		public void Reset()
		{
			throw new SuccessException();
		}
	}

	public class SuccessEnumerator<TValue> : IEnumerator<TValue>
	{
		public TValue Current { get; }

		object IEnumerator.Current => Current;

		public SuccessEnumerator(TValue value)
		{
			Current = value;
		}

		public void Dispose()
		{
			throw new SuccessException();
		}

		public bool MoveNext()
		{
			throw new SuccessException();
		}

		public void Reset()
		{
			throw new SuccessException();
		}
	}
}
