using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using Athene.Extensions;
using Xunit;

namespace Athene.Tests.NonNull
{
	[SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
	public class NonNullEnumeratorTests
	{
		[Fact]
		public void ToNonNull()
		{
			Assert.NotNull(EnumeratorExtensions.ToNonNull(new SuccessEnumerable(null).GetEnumerator()));
		}
		[Fact]
		public void Current_NonNull()
		{
			object obj = new object();
			IEnumerator enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator(obj));

			Assert.Equal(obj, enumerator.Current);
		}

		[Fact]
		public void Current_Null_InvalidOperationException()
		{
			IEnumerator enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator(null));

			Assert.Throws<InvalidOperationException>(() => enumerator.Current);
		}

		[Fact]
		public void MoveNext()
		{
			IEnumerator enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator(null));

			Assert.Throws<SuccessException>(() => enumerator.MoveNext());
		}

		[Fact]
		public void Reset()
		{
			IEnumerator enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator(null));

			Assert.Throws<SuccessException>(() => enumerator.Reset());
		}
	}
}
