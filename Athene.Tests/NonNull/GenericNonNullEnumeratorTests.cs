using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Athene.Extensions;
using Xunit;

namespace Athene.Tests.NonNull
{
	[SuppressMessage("ReSharper", "InvokeAsExtensionMethod")]
	public class GenericNonNullEnumeratorTests
	{
		[Fact]
		public void ToNonNull()
		{
			EnumeratorExtensions.ToNonNull(new SuccessEnumerator<object>(null));
		}

		[Fact]
		public void ToNonNull_NullEnumerator_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => EnumeratorExtensions.ToNonNull<object>(null));
		}

		[Fact]
		public void CurrentOfType_NonNull_Equal()
		{
			object obj = new object();
			IEnumerator<object> enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator<object>(obj));

			Assert.Equal(obj, enumerator.Current);
		}

		[Fact]
		public void CurrentOfType_Null_InvalidOperationException()
		{
			IEnumerator<object> enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator<object>(null));

			Assert.Throws<InvalidOperationException>(() => enumerator.Current);
		}

		[Fact]
		public void Current_NonNull_Equal()
		{
			object obj = new object();
			IEnumerator enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator<object>(obj));

			Assert.Equal(obj, enumerator.Current);
		}

		[Fact]
		public void Current_Null_InvalidOperationException()
		{
			IEnumerator enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator<object>(null));

			Assert.Throws<InvalidOperationException>(() => enumerator.Current);
		}

		[Fact]
		public void MoveNext()
		{
			IEnumerator<object> enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator<object>(null));

			Assert.Throws<SuccessException>(() => enumerator.MoveNext());
		}

		[Fact]
		public void Reset()
		{
			IEnumerator<object> enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator<object>(null));

			Assert.Throws<SuccessException>(() => enumerator.Reset());
		}

		[Fact]
		public void Dispose()
		{
			IEnumerator<object> enumerator = EnumeratorExtensions.ToNonNull(new SuccessEnumerator<object>(null));

			Assert.Throws<SuccessException>(() => enumerator.Dispose());
		}
	}
}
