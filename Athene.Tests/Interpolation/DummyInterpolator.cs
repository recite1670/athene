using System;
using System.Collections.Generic;
using Athene.Components.Interpolators;

namespace Athene.Tests.Interpolation
{
	public class DummyInterpolator : IInterpolator<IExecutionContext>
	{
		public IEnumerable<char> Interpolate(IEnumerable<char> raw, IExecutionContext context)
		{
			throw new NotImplementedException();
		}
	}
}
