using System;
using System.Diagnostics.CodeAnalysis;
using Athene.Components.Interpolators;
using Xunit;

namespace Athene.Tests.Interpolation
{
	public class ConstantEscapeSequenceTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new ConstantEscapeSequence<IExecutionContext>(new char[0], new char[0]);
		}

		[Fact]
		public void Constructor_NullUnescaped_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new ConstantEscapeSequence<IExecutionContext>(null, new char[0]));
		}

		[Fact]
		public void Constructor_NullEscaped_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new ConstantEscapeSequence<IExecutionContext>(new char[0], null));
		}

		[Theory]
		[InlineData("\\n", "\n", "hi\\nbye", "hi\nbye")]
		[InlineData("AAA", "BBB", "ABAABBAAABBB", "ABAABBBBBBBB")]
		[InlineData("092tu iureg443 trpdrs t89ues ut9548y89ue", "aiuyf 8934 y9fgdu943w8ug9i8d ignu439", "weuri 4rw8lg8o092tu iureg443 trpdrs t89ues ut9548y89uef893 8nfu8943", "weuri 4rw8lg8oaiuyf 8934 y9fgdu943w8ug9i8d ignu439f893 8nfu8943")]
		public void Escape_Clean_Equal(string unescaped, string escaped, string raw, string expected)
		{
			ConstantEscapeSequence<IExecutionContext> sequence = new ConstantEscapeSequence<IExecutionContext>(unescaped.ToCharArray(), escaped.ToCharArray());

			Assert.Equal(expected.ToCharArray(), sequence.Interpolate(raw.ToCharArray(), null));
		}
	}
}
