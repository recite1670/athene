using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Xunit;

namespace Athene.Tests.Interpolation
{
	public class DummyEscapeSequenceTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new DummyEscapeSequence(new char[0]);
		}

		[Fact]
		public void Escape_Clean_NotImplementedException()
		{
			DummyEscapeSequence sequence = new DummyEscapeSequence("THROW");

			Assert.Throws<NotImplementedException>(() => sequence.Interpolate("haha funny THROW haha".ToCharArray(), null).ToList());
		}
	}
}
