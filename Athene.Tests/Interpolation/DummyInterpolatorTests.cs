using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Athene.Tests.Interpolation
{
	public class DummyInterpolatorTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new DummyInterpolator();
		}

		[Fact]
		public void Escape_Clean_NotImplementedException()
		{
			DummyInterpolator interpolator = new DummyInterpolator();

			Assert.Throws<NotImplementedException>(() => interpolator.Interpolate(default, default));
		}
	}
}
