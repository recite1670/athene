using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Athene.Tests.Consoles
{
	public class CommandResultTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "UnusedVariable")]
		public void NotFoundGetter_Pass()
		{
			CommandResult result = CommandResult.NotFound;
		}

		[Fact]
		public void Succeed_Clean_Pass()
		{
			CommandResult.Succeed("Command succeeded.");
		}

		[Fact]
		public void Succeed_NullMessage_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => CommandResult.Succeed(null));
		}

		[Fact]
		public void Fail_Clean_Pass()
		{
			CommandResult.Fail("Command failed.");
		}

		[Fact]
		public void Fail_NullMessage_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => CommandResult.Fail(null));
		}

		[Fact]
		public void OutcomeGetter_NotFound_Equal()
		{
			Assert.Equal(ResultOutcome.NotFound, CommandResult.NotFound.Outcome);
		}

		[Fact]
		public void OutcomeGetter_Success_Equal()
		{
			Assert.Equal(ResultOutcome.Success, CommandResult.Succeed("Command succeeded.").Outcome);
		}

		[Fact]
		public void OutcomeGetter_Failed_Equal()
		{
			Assert.Equal(ResultOutcome.Fail, CommandResult.Fail("Command failed.").Outcome);
		}

		[Fact]
		public void MessageGetter_NotFound_Null()
		{
			Assert.Null(CommandResult.NotFound.Message);
		}

		[Fact]
		public void MessageGetter_Success_Equal()
		{
			const string message = "Command succeeded.";
			Assert.Equal(message, CommandResult.Succeed(message).Message);
		}

		[Fact]
		public void MessageGetter_Failed_Equal()
		{
			const string message = "Command failed.";
			Assert.Equal(message, CommandResult.Fail(message).Message);
		}
	}
}
