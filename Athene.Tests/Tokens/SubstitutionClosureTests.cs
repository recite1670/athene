using System;
using System.Diagnostics.CodeAnalysis;
using Athene.Components;
using Athene.Components.Contexts;
using Athene.Components.Tokenizers;
using Athene.Tables;
using Athene.Tests.Commands;
using Xunit;

namespace Athene.Tests.Tokens
{
	public class SubstitutionClosureTests
	{
		[Theory]
		[InlineData("aaa", "bbb")]
		[InlineData("a", "a")]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor(string opening, string closing)
		{
			new SubstitutionClosure<IExecutionContext>(opening, closing);
		}

		[Fact]
		public void Constructor_EmptyOpening_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => new SubstitutionClosure<IExecutionContext>(string.Empty, "aaa"));
		}

		[Fact]
		public void Constructor_EmptyClosing_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => new SubstitutionClosure<IExecutionContext>("aaa", string.Empty));
		}

		[Fact]
		public void Constructor_NullOpening_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new SubstitutionClosure<IExecutionContext>(null, "aaa"));
		}

		[Fact]
		public void Constructor_NullClosing_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new SubstitutionClosure<IExecutionContext>("aaa", null));
		}

		[Theory]
		[InlineData("$(", ")")]
		[InlineData("A", "A")]
		public void Process_Success(string opening, string closing)
		{
			AliasTable<ICommand<IExecutionContext>> commands = new AliasTable<ICommand<IExecutionContext>>
			{
				[new Aliases(nameof(FooBarCommand))] = new FooBarCommand()
			};
			IExecutionContext context = new ComponentExecutionContext(commands, new IdentityTokenizer());
			SubstitutionClosure<IExecutionContext> closure = new SubstitutionClosure<IExecutionContext>(opening, closing);

			Assert.Equal(FooBarCommand.Output.ToCharArray(), closure.Process(nameof(FooBarCommand), context).Value);
		}

		[Theory]
		[InlineData("$(", ")")]
		[InlineData("A", "A")]
		public void Process_NotFound(string opening, string closing)
		{
			IExecutionContext context = new ComponentExecutionContext(new AliasTable<ICommand<IExecutionContext>>(), new IdentityTokenizer());
			SubstitutionClosure<IExecutionContext> closure = new SubstitutionClosure<IExecutionContext>(opening, closing);

			Assert.Throws<CommandException>(() => closure.Process("aaaaa", context));
		}

		[Theory]
		[InlineData("$(", ")")]
		[InlineData("A", "A")]
		public void Process_Failed(string opening, string closing)
		{
			AliasTable<ICommand<IExecutionContext>> commands = new AliasTable<ICommand<IExecutionContext>>
			{
				[new Aliases(nameof(CommandExceptionCommand))] = new CommandExceptionCommand()
			};
			IExecutionContext context = new ComponentExecutionContext(commands, new IdentityTokenizer());
			SubstitutionClosure<IExecutionContext> closure = new SubstitutionClosure<IExecutionContext>(opening, closing);

			Assert.Throws<CommandException>(() => closure.Process(nameof(CommandExceptionCommand), context));
		}

		[Fact]
		public void Dispose()
		{
			SubstitutionClosure<IExecutionContext> closure = new SubstitutionClosure<IExecutionContext>("aaa", "aaa");
			closure.Dispose();

			Assert.Throws<ObjectDisposedException>(() => closure.Process(null, null));
		}
	}
}
