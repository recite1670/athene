using System;
using System.Diagnostics.CodeAnalysis;
using Athene.Components.Tokenizers;
using Xunit;

namespace Athene.Tests.Tokens
{
	public class TokenTests
	{
		[Theory]
		[InlineData("", false)]
		[InlineData("", true)]
		[InlineData("aaa", false)]
		[InlineData("aaa", true)]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor(string value, bool interpolate)
		{
			new Token(value, interpolate);
		}

		[Fact]
		public void Constructor_NullValue_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new Token(null, false));
		}

		[Theory]
		[InlineData("")]
		[InlineData("aaa")]
		[InlineData("bbb")]
		public void ValueGetter(string value)
		{
			Token token = new Token(value, false);

			Assert.Equal(value, token.Value);
		}

		[Theory]
		[InlineData(false)]
		[InlineData(true)]
		public void InterpolateGetter(bool interpolate)
		{
			Token token = new Token("aaa", interpolate);

			Assert.Equal(interpolate, token.Interpolate);
		}

		[Theory]
		[InlineData("", "aaa")]
		[InlineData("aaa", "bbb")]
		[InlineData("aaa", "aaa")]
		public void Alter(string initial, string altered)
		{
			Token token = new Token(initial, false);
			token = token.Alter(altered);

			Assert.Equal(altered, token.Value);
		}
	}
}
