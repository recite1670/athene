using System;
using System.Collections.Generic;
using Athene.Components.Tokenizers;

namespace Athene.Tests.Tokens
{
	public class DummyTokenizer : ITokenizer<IExecutionContext>
	{
		public IEnumerable<Token> Tokenize(IEnumerable<char> raw, IExecutionContext context)
		{
			throw new NotImplementedException();
		}
	}
}
