using Athene.Components.Tokenizers;
using Xunit;

namespace Athene.Tests.Tokens
{
	public class EmptyTokenizerTests
	{
		[Fact]
		public void Tokenize_Any_Equal()
		{
			EmptyTokenizer tokenizer = new EmptyTokenizer();

			Assert.Equal(new Token[0], tokenizer.Tokenize(null, default));
		}
	}
}
