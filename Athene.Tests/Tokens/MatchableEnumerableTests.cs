using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Components.Tokenizers;
using Xunit;

namespace Athene.Tests.Tokens
{
	public class MatchableEnumerableTests
	{
		[Theory]
		[InlineData(new[] { 0, 1, 2 })]
		[InlineData(new[] { 10, 5, 2 })]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor(int[] wrapped)
		{
			new MatchableEnumerable<int>(wrapped);
		}

		[Fact]
		public void Constructor_EmptyWrapped_ArgumentException()
		{
			Assert.Throws<ArgumentException>(() => new MatchableEnumerable<int>(new int[0]));
		}

		[Fact]
		public void Constructor_NullWrapped_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new MatchableEnumerable<int>(null));
		}

		[Theory]
		[InlineData(0)]
		[InlineData(2)]
		[InlineData(-2)]
		[InlineData(int.MaxValue)]
		[InlineData(int.MinValue)]
		public void EscapeValue(int escapeValue)
		{
			MatchableEnumerable<int> enumerable = new MatchableEnumerable<int>(new[] { 1, 2, 3 });

			Assert.Equal(default, enumerable.EscapeValue);

			enumerable.EscapeValue = escapeValue;
			Assert.Equal(escapeValue, enumerable.EscapeValue);
		}

		[Theory]
		[InlineData(0)]
		[InlineData(2)]
		[InlineData(int.MaxValue)]
		public void Escapable(int escapable)
		{
			MatchableEnumerable<int> enumerable = new MatchableEnumerable<int>(new[] { 1, 2, 3 });

			Assert.Equal(default, enumerable.Escapable);

			enumerable.Escapable = escapable;
			Assert.Equal(escapable, enumerable.Escapable);
		}

		[Theory]
		[InlineData(-1)]
		[InlineData(-2)]
		[InlineData(int.MinValue)]
		public void Escapable_NegativeValue_ArgumentOutOfRangeException(int escapable)
		{
			MatchableEnumerable<int> enumerable = new MatchableEnumerable<int>(new[] { 1, 2, 3 });

			Assert.Throws<ArgumentOutOfRangeException>(() => enumerable.Escapable = escapable);
		}

		[Theory]
		[InlineData(new[] { 0 }, 0, 0)]
		[InlineData(new[] { 0, 1 }, 0, 1)]
		[InlineData(new[] { 9, 6, 3 }, 9, 1)]
		public void Advance(int[] wrapped, int escapable, int escapeValue)
		{
			MatchableEnumerable<int> enumerable = new MatchableEnumerable<int>(wrapped)
			{
				Escapable = escapable,
				EscapeValue = escapeValue
			};

			List<int> buffer = new List<int>();

			using (IEnumerator<int> enumerator = wrapped.AsEnumerable().GetEnumerator())
			{
				int matched;

				for (int i = 0; i < wrapped.Length - 1; ++i)
				{
					Assert.True(enumerator.MoveNext());
					buffer.Add(enumerator.Current);

					Assert.False(enumerable.Advance(enumerator.Current, out matched));
					buffer.RemoveRange(buffer.Count - matched, matched);
				}

				Assert.True(enumerator.MoveNext());
				buffer.Add(enumerator.Current);

				Assert.True(enumerable.Advance(enumerator.Current, out matched));
				buffer.RemoveRange(buffer.Count - matched, matched);

				Assert.Empty(buffer);

				Assert.False(enumerator.MoveNext());
			}
		}

		[Theory]
		[InlineData(new[] { 0 }, 0, 0)]
		[InlineData(new[] { 0, 1 }, 1, 1)]
		[InlineData(new[] { 9, 6, 3 }, 6, 1)]
		public void Advance_EscapeValue_InvalidOperationException(int[] wrapped, int escapeValue, int badIndex)
		{
			MatchableEnumerable<int> enumerable = new MatchableEnumerable<int>(wrapped)
			{
				Escapable = 1,
				EscapeValue = escapeValue
			};

			using (IEnumerator<int> enumerator = wrapped.AsEnumerable().GetEnumerator())
			{
				for (int i = 0; i < badIndex; ++i)
				{
					Assert.True(enumerator.MoveNext());
					Assert.False(enumerable.Advance(enumerator.Current, out _));
				}

				Assert.True(enumerator.MoveNext());
				Assert.Throws<InvalidOperationException>(() => enumerable.Advance(enumerator.Current, out _));
			}
		}

		[Fact]
		public void Reset()
		{
			MatchableEnumerable<int> enumerable = new MatchableEnumerable<int>(new[] { 0, 1, 2 });

			enumerable.Reset();
		}

		[Fact]
		public void Dispose()
		{
			MatchableEnumerable<int> enumerable = new MatchableEnumerable<int>(new[] { 0, 1, 2 });

			enumerable.Dispose();

			Assert.Throws<ObjectDisposedException>(() => enumerable.Escapable);
			Assert.Throws<ObjectDisposedException>(() => enumerable.Escapable);
			Assert.Throws<ObjectDisposedException>(() => enumerable.Escapable = 0);
			Assert.Throws<ObjectDisposedException>(() => enumerable.EscapeValue);
			Assert.Throws<ObjectDisposedException>(() => enumerable.EscapeValue = 0);
			Assert.Throws<ObjectDisposedException>(() => enumerable.Advance(0, out _));
			Assert.Throws<ObjectDisposedException>(() => enumerable.Reset());
		}
	}
}
