using System.Collections.Generic;
using System.Linq;
using Athene.Components.Tokenizers;

namespace Athene.Tests.Tokens
{
	public class IdentityTokenizer : ITokenizer<IExecutionContext>
	{
		public IEnumerable<Token> Tokenize(IEnumerable<char> raw, IExecutionContext context)
		{
			return new[] { new Token(new string(raw.ToArray()), true) };
		}
	}
}
