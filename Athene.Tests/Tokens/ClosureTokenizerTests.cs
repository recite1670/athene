using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Athene.Components.Tokenizers;
using Xunit;

namespace Athene.Tests.Tokens
{
	public class ClosureTokenizerTests
	{
		[Fact]
		public void DefaultWrappedClosuresGetter()
		{
			Assert.NotNull(ClosureTokenizer<IExecutionContext>.DefaultWrappedClosures);
		}

		[Fact]
		public void DefaultDelimitedClosuresGetter()
		{
			Assert.NotNull(ClosureTokenizer<IExecutionContext>.DefaultDelimitedClosure);
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new ClosureTokenizer<IExecutionContext>();
			new ClosureTokenizer<IExecutionContext>(new IWrappedClosure<IExecutionContext>[0], new RawDelimitedClosure<IExecutionContext>("'"));
		}

		[Fact]
		public void Constructor_NullWrappedClosures_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new ClosureTokenizer<IExecutionContext>(null, new RawDelimitedClosure<IExecutionContext>("'")));
		}

		[Fact]
		public void Constructor_NullDelimiterClosure_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new ClosureTokenizer<IExecutionContext>(null, new RawDelimitedClosure<IExecutionContext>("'")));
		}

		[Theory]
		[InlineData("a b", new[] { "a", "b" })]
		[InlineData("a b c", new[] { "a", "b", "c" })]
		[InlineData("a b c d", new[] { "a", "b", "c", "d" })]
		[InlineData("a 'b' 'c' d", new[] { "a", "b", "c", "d" })]
		[InlineData("a \"b\" \"c\" d", new[] { "a", "b", "c", "d" })]
		[InlineData("'a b' c d", new[] { "a b", "c", "d" })]
		[InlineData("a 'b c' d", new[] { "a", "b c", "d" })]
		[InlineData("\"a b\" c d", new[] { "a b", "c", "d" })]
		[InlineData("a \"b c\" d", new[] { "a", "b c", "d" })]
		[InlineData("a \\'b c\\' d", new[] { "a", "'b", "c'", "d" })]
		[InlineData("a \\\"b c\\\" d", new[] { "a", "\"b", "c\"", "d" })]
		public void Tokenize(string toTokenize, string[] expected)
		{
			ClosureTokenizer<IExecutionContext> tokenizer = new ClosureTokenizer<IExecutionContext>();

			Assert.Equal(expected, tokenizer.Tokenize(toTokenize, null).Select(x => new string(x.Value.ToArray())));
		}
	}
}
