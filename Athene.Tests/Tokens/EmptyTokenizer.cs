using System.Collections.Generic;
using Athene.Components.Tokenizers;

namespace Athene.Tests.Tokens
{
	public class EmptyTokenizer : ITokenizer<IExecutionContext>
	{
		public IEnumerable<Token> Tokenize(IEnumerable<char> raw, IExecutionContext context)
		{
			return new Token[0];
		}
	}
}
