using System;
using System.Diagnostics.CodeAnalysis;
using Athene.Tables;
using Athene.Tests.Commands;
using Xunit;

namespace Athene.Tests.Tables
{
	public class AliasTableTests
	{
		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor()
		{
			new AliasTable<int>();
		}

		[Fact]
		[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
		public void Constructor_EqualityComparer()
		{
			new AliasTable<int>(StringComparer.InvariantCultureIgnoreCase);
		}

		[Fact]
		public void Constructor_NullEqualityComparer_ArgumentNullException()
		{
			Assert.Throws<ArgumentNullException>(() => new AliasTable<int>(null));
		}

		[Fact]
		public void AliasesGetter_NoCommands_Empty()
		{
			AliasTable<int> table = new AliasTable<int>();

			Assert.NotNull(table.Aliases);
			Assert.Equal(new Aliases[0], table.Aliases);
		}

		[Fact]
		public void ValuesGetter_NoValues_Empty()
		{
			AliasTable<int> table = new AliasTable<int>();

			Assert.NotNull(table.Values);
			Assert.Equal(new int[0], table.Values);
		}

		[Fact]
		public void AliasIndexerGetter_NoValues_Default()
		{
			AliasTable<int> table = new AliasTable<int>();

			Assert.Equal(default, table["dummy"]);
		}

		[Fact]
		public void AliasIndexerGetter_OneValue_Equal()
		{
			const string alias = nameof(DummyCommand);
			const int value = 2038;
			AliasTable<int> table = new AliasTable<int>
			{
				[new Aliases(alias)] = value
			};

			Assert.Equal(value, table[alias]);
		}

		[Fact]
		public void AliasesIndexerGetter_NoValues_Default()
		{
			AliasTable<int> table = new AliasTable<int>();

			Assert.Equal(default, table[new Aliases("dummy")]);
		}

		[Fact]
		public void AliasesIndexerGetter_OneValue_Equal()
		{
			const int value = 2038;
			Aliases aliases = new Aliases(nameof(DummyCommand));
			AliasTable<int> table = new AliasTable<int>
			{
				[aliases] = value
			};

			Assert.Equal(value, table[aliases]);
		}

		[Fact]
		public void AliasIndexerGetter_NullAlias_ArgumentNullException()
		{
			AliasTable<int> table = new AliasTable<int>();

			Assert.Throws<ArgumentNullException>(() => table[null]);
		}

		[Fact]
		public void Add_OneValue_Pass()
		{
			AliasTable<int> table = new AliasTable<int>();

			table.Add(new Aliases("dummy"), 2038);
		}

		[Fact]
		public void Add_ConflictingCommands_InvalidOperationException()
		{
			const string alias = "dummy";
			AliasTable<int> table = new AliasTable<int>
			{
				[new Aliases(alias)] = 2038
			};

			Assert.Throws<InvalidOperationException>(() => table.Add(new Aliases(alias), 1970));
		}

		[Fact]
		public void Remove_OneValue_Pass()
		{
			Aliases aliases = new Aliases("dummy");
			AliasTable<int> table = new AliasTable<int>
			{
				[aliases] = 2038
			};

			Assert.True(table.Remove(aliases));
		}
	}
}
