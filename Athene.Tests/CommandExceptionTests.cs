using System;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Athene.Tests
{
	[SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
	public class CommandExceptionTests
	{
		[Fact]
		public void Constructor()
		{
			new CommandException("Oh no, something went wrong!");
		}

		[Fact]
		public void Constructor_NonNullInner()
		{
			new CommandException("Oh no, something went wrong!", new Exception());
		}
	}
}
