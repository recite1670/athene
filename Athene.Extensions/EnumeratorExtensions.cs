using System;
using System.Collections;
using System.Collections.Generic;

namespace Athene.Extensions
{
	/// <summary>
	/// 	Extensions for <see cref="IEnumerator{T}"/>.
	/// </summary>
	public static class EnumeratorExtensions
	{
		/// <summary>
		/// 	Converts this enumerator to an enumerator that cannot yield a null value.
		/// </summary>
		/// <param name="enumerator">The enumerator to wrap.</param>
		/// <returns>An enumerator that cannot yield a null value.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerator"/> is <see langword="null"/>.</exception>
		public static IEnumerator ToNonNull(this IEnumerator enumerator)
		{
			if (enumerator == null)
			{
				throw new ArgumentNullException(nameof(enumerator));
			}

			return new NonNullEnumerator(enumerator);
		}

		/// <summary>
		/// 	Converts this enumerator to an enumerator that cannot yield a null value.
		/// </summary>
		/// <param name="enumerator">The enumerator to wrap.</param>
		/// <typeparam name="TValue">The generic type of the enumerator.</typeparam>
		/// <returns>An enumerator that cannot yield a null value.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerator"/> is <see langword="null"/>.</exception>
		public static IEnumerator<TValue> ToNonNull<TValue>(this IEnumerator<TValue> enumerator) where TValue : class
		{
			if (enumerator == null)
			{
				throw new ArgumentNullException(nameof(enumerator));
			}

			return new NonNullEnumerator<TValue>(enumerator);
		}

		private static bool TryTakeNextInternal<TValue>(IEnumerator<TValue> enumerator, out TValue value)
		{
			if (!enumerator.MoveNext())
			{
				value = default;
				return false;
			}

			value = enumerator.Current;
			return true;
		}

		/// <summary>
		/// 	Advances an enumerator and takes the element.
		/// </summary>
		/// <param name="enumerator">An unfinished enumerator.</param>
		/// <typeparam name="TValue">The type of the enumerator's values.</typeparam>
		/// <returns>The next element from the enumerator.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerator"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException"><paramref name="enumerator"/> has already ended.</exception>
		public static TValue TakeNext<TValue>(this IEnumerator<TValue> enumerator)
		{
			if (enumerator == null)
			{
				throw new ArgumentNullException(nameof(enumerator));
			}

			if (!TryTakeNextInternal(enumerator, out TValue value))
			{
				throw new ArgumentException("Enumeration has already ended.");
			}

			return value;
		}

		/// <summary>
		/// 	Attempts to advance an enumerator and take the element.
		/// </summary>
		/// <param name="enumerator">An enumerator.</param>
		/// <param name="value">The value of the next element.</param>
		/// <typeparam name="TValue">The type of the enumerator's values.</typeparam>
		/// <returns>Whether or not the next element was retrieved..</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerator"/> is <see langword="null"/>.</exception>
		public static bool TryTakeNext<TValue>(this IEnumerator<TValue> enumerator, out TValue value)
		{
			if (enumerator == null)
			{
				throw new ArgumentNullException(nameof(enumerator));
			}

			return TryTakeNextInternal(enumerator, out value);
		}

		/// <summary>
		/// 	Advances an enumerator by an amount and yields each element.
		/// </summary>
		/// <param name="enumerator">An enumerator that will not be finished until it has been advanced by <paramref name="count"/> or more.</param>
		/// <param name="count">The number of elements to yield.</param>
		/// <typeparam name="TValue">The type of the enumerator's value.</typeparam>
		/// <returns>Elements from the enumerator, the count of which is determined by the count provided.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerator"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="count"/> is negative.</exception>
		/// <exception cref="ArgumentException"><paramref name="enumerator"/> finished before all elements were yielded.</exception>
		public static IEnumerable<TValue> Take<TValue>(this IEnumerator<TValue> enumerator, int count)
		{
			if (enumerator == null)
			{
				throw new ArgumentNullException(nameof(enumerator));
			}

			if (count < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(count), count, "Count cannot be negative.");
			}

			for (int i = 0; i < count; ++i)
			{
				if (!enumerator.MoveNext())
				{
					throw new ArgumentException("Enumeration ended before the end of the count.");
				}

				yield return enumerator.Current;
			}
		}

		private static bool TryTakeToArrayInternal<TValue>(IEnumerator<TValue> enumerator, int count, out TValue[] values)
		{
			TValue[] buffer = new TValue[count];

			for (int i = 0; i < count; ++i)
			{
				if (!enumerator.MoveNext())
				{
					values = null;
					return false;
				}

				buffer[i] = enumerator.Current;
			}

			values = buffer;
			return true;
		}

		/// <summary>
		/// 	Advances an enumerator by an amount and stores each element in an array.
		/// </summary>
		/// <param name="enumerator">An enumerator that will not be finished until it has been advanced by <paramref name="count"/> or more.</param>
		/// <param name="count">The number of elements to store.</param>
		/// <typeparam name="TValue">The type of the enumerator's value.</typeparam>
		/// <returns>An array of elements from the enumerator, the length of which is determined by the count provided.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerator"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="count"/> is negative.</exception>
		/// <exception cref="ArgumentException"><paramref name="enumerator"/> finished before all elements were stored.</exception>
		public static TValue[] TakeToArray<TValue>(this IEnumerator<TValue> enumerator, int count)
		{
			if (enumerator == null)
			{
				throw new ArgumentNullException(nameof(enumerator));
			}

			if (count < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(count), count, "Count cannot be negative.");
			}

			if (!TryTakeToArrayInternal(enumerator, count, out TValue[] values))
			{
				throw new ArgumentException("Enumeration ended before the end of the count.");
			}

			return values;
		}

		/// <summary>
		/// 	Attempts to advance an enumerator by an amount and store each element in an array.
		/// </summary>
		/// <param name="enumerator">An enumerator.</param>
		/// <param name="count">The number of elements to store.</param>
		/// <param name="values">An array of elements from the enumerator, the length of which is determined by the count provided.</param>
		/// <typeparam name="TValue">The type of the enumerator's value.</typeparam>
		/// <returns>Whether or not all elements were able to be stored.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerator"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="count"/> is negative.</exception>
		public static bool TryTakeToArray<TValue>(this IEnumerator<TValue> enumerator, int count, out TValue[] values)
		{
			if (enumerator == null)
			{
				throw new ArgumentNullException(nameof(enumerator));
			}

			if (count < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(count), count, "Count cannot be negative.");
			}

			return TryTakeToArrayInternal(enumerator, count, out values);
		}
	}
}
