using System;
using System.Collections;
using System.Collections.Generic;

namespace Athene.Extensions
{
	/// <summary>
	/// 	Extensions for <see cref="IEnumerator{T}"/>.
	/// </summary>
	public static class EnumerableExtensions
	{
		/// <summary>
		/// 	Converts this enumerable to an enumerable whose enumerators cannot yield a null value.
		/// </summary>
		/// <param name="enumerable">The enumerable to wrap.</param>
		/// <returns>An enumerable whose enumerators cannot yield a null value.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerable"/> is <see langword="null"/>.</exception>
		public static IEnumerable ToNonNull(this IEnumerable enumerable)
		{
			if (enumerable == null)
			{
				throw new ArgumentNullException(nameof(enumerable));
			}

			return new NonNullEnumerable(enumerable);
		}

		/// <summary>
		/// 	Converts this enumerable to an enumerable whose enumerators cannot yield a null value.
		/// </summary>
		/// <param name="enumerable">The enumerable to wrap.</param>
		/// <typeparam name="TValue">The generic type of the enumerable.</typeparam>
		/// <returns>An enumerable whose enumerators cannot yield a null value.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="enumerable"/> is <see langword="null"/>.</exception>
		public static IEnumerable<TValue> ToNonNull<TValue>(this IEnumerable<TValue> enumerable) where TValue : class
		{
			if (enumerable == null)
			{
				throw new ArgumentNullException(nameof(enumerable));
			}

			return new NonNullEnumerable<TValue>(enumerable);
		}
	}
}
