# About
Athene is a command library whose goal is to make writing Unix/GNU styled command-line commands as simple as possible.  
While reflection could be used to reduce the boilerplate, it would also make it harder to read and use on different platforms due to the nature of reflection.
