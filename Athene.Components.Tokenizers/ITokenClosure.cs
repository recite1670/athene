using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A grouper for token characters.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public interface ITokenClosure<in TContext> where TContext : IExecutionContext
	{
		/// <summary>
		///		Converts the contents of a token into a <see cref="Token"/>.
		/// </summary>
		/// <param name="token">The contents of the token.</param>
		/// <param name="context">The currently running execution context.</param>
		/// <returns>A <see cref="Token"/> that represents the contents fed.</returns>
		Token Process(IEnumerable<char> token, TContext context);
	}
}
