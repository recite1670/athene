using System;
using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	An object that can match values to an advancing enumeration.
	/// </summary>
	/// <typeparam name="TValue">The value of the enumerable.</typeparam>
	public class MatchableEnumerable<TValue> : IDisposable
	{
		private static readonly EqualityComparer<TValue> _comparer = EqualityComparer<TValue>.Default;

		private readonly IEnumerator<TValue> _wrapped;
		private int _count;

		private TValue _escapeValue;
		private int _escapable;
		private int _escaped;

		/// <summary>
		/// 	Whether or not this object has been disposed and should throw upon any calls to methods or properties aside from <seealso cref="Disposed"/>, <seealso cref="Dispose()"/>, and <seealso cref="Dispose(bool)"/>.
		/// </summary>
		protected bool Disposed { get; private set; }

		/// <summary>
		/// 	The value that will cause any value after it to be ignored.
		/// </summary>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public TValue EscapeValue
		{
			get
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(MatchableEnumerable<TValue>));
				}

				return _escapeValue;
			}
			set
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(MatchableEnumerable<TValue>));
				}

				_escapeValue = value;
			}
		}

		/// <summary>
		///
		/// </summary>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="value"/> was negative.</exception>
		public int Escapable
		{
			get
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(MatchableEnumerable<TValue>));
				}

				return _escapable;
			}
			set
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(MatchableEnumerable<TValue>));
				}

				if (value < 0)
				{
					throw new ArgumentOutOfRangeException(nameof(value), value, "Value was negative.");
				}

				_escapable = value;
			}
		}

		/// <summary>
		/// 	Constructs an instance of <see cref="MatchableEnumerable{TValue}"/>.
		/// </summary>
		/// <param name="wrapped">The enumerable to advance against.</param>
		/// <exception cref="ArgumentNullException"><paramref name="wrapped"/> is <see langword="null"/>.</exception>
		/// <exception cref="ArgumentException"><paramref name="wrapped"/> is empty.</exception>
		public MatchableEnumerable(IEnumerable<TValue> wrapped)
		{
			if (wrapped == null)
			{
				throw new ArgumentNullException(nameof(wrapped));
			}

			_wrapped = wrapped.GetEnumerator();
			_wrapped.Reset();

			if (!_wrapped.MoveNext())
			{
				throw new ArgumentException("Enumerable was empty.", nameof(wrapped));
			}
		}

		/// <summary>
		/// 	Finalizes an instance of <see cref="MatchableEnumerable{TValue}"/>.
		/// </summary>
		~MatchableEnumerable()
		{
			Dispose(false);
		}

		/// <inheritdoc cref="IDisposable.Dispose"/>
		/// <param name="disposing">Whether or not the method is being disposed via <seealso cref="Dispose()"/> (true) or via a finalizer (false).</param>
		protected virtual void Dispose(bool disposing)
		{
			if (Disposed)
			{
				return;
			}

			if (disposing)
			{
				_wrapped.Dispose();
			}

			Disposed = true;
		}

		/// <summary>
		/// 	Advances the underlying enumerator and compares it against a fed value. If a match is completed, <seealso cref="Reset"/> will automatically be called.
		/// </summary>
		/// <param name="value">The value to compare against.</param>
		/// <param name="matched">If not a complete match, the characters to remove. Otherwise, the size of the match.</param>
		/// <returns>Whether or not a complete match was made.</returns>
		/// <exception cref="InvalidOperationException">The underlying enumeration has the same value as the escape value.</exception>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public bool Advance(TValue value, out int matched)
		{
			if (Disposed)
			{
				throw new ObjectDisposedException(nameof(MatchableEnumerable<TValue>));
			}

			if (_escapable > 0)
			{
				if (_escaped > 0)
				{
					--_escaped;

					matched = 0;
					return false;
				}

				if (_comparer.Equals(value, _escapeValue))
				{
					if (_comparer.Equals(_escapeValue, _wrapped.Current))
					{
						throw new InvalidOperationException("The escape value is identical to the matching enumerable.");
					}

					Reset();

					_escaped += _escapable;

					matched = 1;
					return false;
				}
			}

			if (_comparer.Equals(value, _wrapped.Current))
			{
				++_count;

				if (!_wrapped.MoveNext())
				{
					matched = _count;

					Reset();

					return true;
				}
			}

			matched = 0;
			return false;
		}

		/// <summary>
		/// 	Resets the advancements called.
		/// </summary>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public void Reset()
		{
			if (Disposed)
			{
				throw new ObjectDisposedException(nameof(MatchableEnumerable<TValue>));
			}

			_wrapped.Reset();
			_wrapped.MoveNext();

			_count = 0;
		}

		/// <inheritdoc cref="IDisposable.Dispose"/>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
