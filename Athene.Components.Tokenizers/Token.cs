using System;
using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	Represents a set of characters to be processed by a command.
	/// </summary>
	public readonly struct Token
	{
		/// <summary>
		/// 	Whether or not the token should be interpolated (if available) before being sent to the command.
		/// </summary>
		public bool Interpolate { get; }

		/// <summary>
		/// 	The set of characters this token represents.
		/// </summary>
		public IEnumerable<char> Value { get; }

		/// <summary>
		/// 	Constructs an instance of <see cref="Token"/>.
		/// </summary>
		/// <param name="value">The set of characters this token represents.</param>
		/// <param name="interpolate">Whether or not the token should be interpolate (if available) before being sent to the command.</param>
		public Token(IEnumerable<char> value, bool interpolate)
		{
			Value = value ?? throw new ArgumentNullException(nameof(value));
			Interpolate = interpolate;
		}

		/// <summary>
		/// 	Constructs a new token with altered properties.
		/// </summary>
		/// <param name="value">The value of <see cref="Value"/> in the new token.</param>
		/// <param name="interpolate">The value of <see cref="Interpolate"/> in the new token.</param>
		/// <returns>The token with altered properties from this token.</returns>
		public Token Alter(IEnumerable<char> value = null, bool? interpolate = null)
		{
			return new Token(value ?? Value, interpolate ?? Interpolate);
		}
	}
}
