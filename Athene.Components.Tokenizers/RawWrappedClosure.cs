using System;
using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A wrapped closure that uses the raw value of the character grouping as the token value.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public sealed class RawWrappedClosure<TContext> : EnumerableWrappedClosure<TContext> where TContext : IExecutionContext
	{
		private bool _interpolate;

		/// <summary>
		/// 	Whether or not the character grouping is able to be interpolated.
		/// </summary>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public bool Interpolate
		{
			get
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(RawWrappedClosure<TContext>));
				}

				return _interpolate;
			}
			set
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(RawWrappedClosure<TContext>));
				}

				_interpolate = value;
			}
		}

		/// <summary>
		/// 	Constructs an instance of <see cref="RawWrappedClosure{TContext}"/>.
		/// </summary>
		/// <param name="opening">The characters not included in the token value at the start of a token.</param>
		/// <param name="closing">The characters not included in the token value at the end of a token.</param>
		/// <exception cref="ArgumentNullException"><paramref name="opening"/> or <paramref name="closing"/> is <see langword="null"/>.</exception>
		public RawWrappedClosure(IEnumerable<char> opening, IEnumerable<char> closing) : base(opening ?? throw new ArgumentNullException(nameof(opening)), closing ?? throw new ArgumentNullException(nameof(closing)))
		{
		}

		/// <summary>
		/// 	Finalizes an instance of <see cref="RawWrappedClosure{TContext}"/>.
		/// </summary>
		~RawWrappedClosure()
		{
			Dispose(false);
		}

		/// <inheritdoc cref="EnumerableWrappedClosure{TContext}.Process"/>
		/// <exception cref="ArgumentNullException"><paramref name="token"/> is <see langword="null"/>.</exception>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public override Token Process(IEnumerable<char> token, TContext context)
		{
			if (Disposed)
			{
				throw new ObjectDisposedException(nameof(RawWrappedClosure<TContext>));
			}

			if (token == null)
			{
				throw new ArgumentNullException(nameof(token));
			}

			return new Token(token, Interpolate);
		}
	}
}
