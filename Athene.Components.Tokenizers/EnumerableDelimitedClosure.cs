using System;
using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A delimited closure that uses an enumerable of characters as the delimiter.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public abstract class EnumerableDelimitedClosure<TContext> : IDelimitedClosure<TContext>, IDisposable where TContext : IExecutionContext
	{
		private readonly MatchableEnumerable<char> _delimiter;

		/// <summary>
		/// 	Whether or not this object has been disposed and should throw upon any calls to methods or properties aside from <seealso cref="Disposed"/>, <seealso cref="Dispose()"/>, and <seealso cref="Dispose(bool)"/>.
		/// </summary>
		protected bool Disposed { get; private set; }

		/// <summary>
		/// 	The escape character for this closure.
		/// </summary>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public char? EscapeCharacter
		{
			get
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(EnumerableDelimitedClosure<TContext>));
				}

				return _delimiter.Escapable > 0 ? _delimiter.EscapeValue : (char?) null;
			}
			set
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(EnumerableDelimitedClosure<TContext>));
				}

				if (value.HasValue)
				{
					_delimiter.EscapeValue = value.Value;
					_delimiter.Escapable = 1;
				}
				else
				{
					_delimiter.EscapeValue = default;
					_delimiter.Escapable = 0;
				}
			}
		}

		/// <summary>
		/// 	Constructs an instance of <see cref="EnumerableDelimitedClosure{TContext}"/>.
		/// </summary>
		/// <param name="delimiter">The delimiting characters between tokens.</param>
		/// <exception cref="ArgumentNullException"><paramref name="delimiter"/> is <see langword="null"/>.</exception>
		protected EnumerableDelimitedClosure(IEnumerable<char> delimiter)
		{
			if (delimiter == null)
			{
				throw new ArgumentNullException(nameof(delimiter));
			}

			_delimiter = new MatchableEnumerable<char>(delimiter);
		}

		/// <summary>
		/// 	Finalizes an instance of <see cref="EnumerableDelimitedClosure{TContext}"/>.
		/// </summary>
		~EnumerableDelimitedClosure()
		{
			Dispose(false);
		}

		/// <inheritdoc cref="ITokenClosure{TContext}.Process"/>
		public abstract Token Process(IEnumerable<char> token, TContext context);

		/// <inheritdoc cref="IDelimitedClosure{TContext}.AdvanceDelimiter"/>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public bool AdvanceDelimiter(char c, TContext context, out int toRemove)
		{
			if (Disposed)
			{
				throw new ObjectDisposedException(nameof(EnumerableDelimitedClosure<TContext>));
			}

			return _delimiter.Advance(c, out toRemove);
		}

		/// <inheritdoc cref="IDisposable.Dispose"/>
		/// <param name="disposing">Whether or not the method is being disposed via <seealso cref="Dispose()"/> (true) or via a finalizer (false).</param>
		protected virtual void Dispose(bool disposing)
		{
			if (Disposed)
			{
				return;
			}

			if (disposing)
			{
				_delimiter?.Dispose();
			}

			Disposed = true;
		}

		/// <inheritdoc cref="IDisposable.Dispose"/>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
