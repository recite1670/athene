using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A tokenizer that uses <see cref="ITokenClosure{TContext}"/> objects to tokenize.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public class ClosureTokenizer<TContext> : ITokenizer<TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	The default Bash wrapped closures.
		/// </summary>
		public static IEnumerable<IWrappedClosure<TContext>> DefaultWrappedClosures { get; } = new ReadOnlyCollection<IWrappedClosure<TContext>>(new IWrappedClosure<TContext>[]
		{
			new RawWrappedClosure<TContext>("\'", "\'")
			{
				EscapeCharacter = '\\'
			},
			new RawWrappedClosure<TContext>("\"", "\"")
			{
				Interpolate = true,
				EscapeCharacter = '\\'
			},
			new SubstitutionClosure<TContext>("$(", ")")
			{
				EscapeCharacter = '\\'
			}
		});

		/// <summary>
		/// 	The default Bash delimited closure.
		/// </summary>
		public static IDelimitedClosure<TContext> DefaultDelimitedClosure { get; } = new RawDelimitedClosure<TContext>(" ")
		{
			Interpolate = true,
			EscapeCharacter = '\\'
		};

		private readonly IEnumerable<IWrappedClosure<TContext>> _wrappedClosures;
		private readonly IDelimitedClosure<TContext> _delimitedClosure;

		/// <summary>
		/// 	Constructs an instance of <see cref="ClosureTokenizer{TContext}"/> with the default closures.
		/// </summary>
		public ClosureTokenizer() : this(DefaultWrappedClosures, DefaultDelimitedClosure)
		{
		}

		/// <summary>
		/// 	Constructs an instance of <see cref="ClosureTokenizer{TContext}"/>.
		/// </summary>
		/// <param name="wrappedClosures">The wrapped closures of the tokenizer.</param>
		/// <param name="delimitedClosure"></param>
		/// <exception cref="ArgumentNullException"><paramref name="wrappedClosures"/> or <paramref name="delimitedClosure"/> is <see langword="null"/>.</exception>
		public ClosureTokenizer(IEnumerable<IWrappedClosure<TContext>> wrappedClosures, IDelimitedClosure<TContext> delimitedClosure)
		{
			if (wrappedClosures == null)
			{
				throw new ArgumentNullException(nameof(wrappedClosures));
			}

			if (delimitedClosure == null)
			{
				throw new ArgumentNullException(nameof(delimitedClosure));
			}

			_wrappedClosures = wrappedClosures;
			_delimitedClosure = delimitedClosure;
		}

		/// <inheritdoc cref="ITokenizer{TContext}.Tokenize"/>
		public IEnumerable<Token> Tokenize(IEnumerable<char> raw, TContext context)
		{
			IWrappedClosure<TContext> currentWrapped = null;
			List<char> currentToken = new List<char>();

			using (IEnumerator<char> rawEnumerator = raw.GetEnumerator())
			{
				while (rawEnumerator.MoveNext())
				{
					char c = rawEnumerator.Current;

					currentToken.Add(c);

					if (currentWrapped != null)
					{
						bool closingFinished = currentWrapped.AdvanceClosing(c, context, out int closingTrail);

						currentToken.RemoveRange(currentToken.Count - closingTrail, closingTrail);

						if (!closingFinished)
						{
							continue;
						}

						yield return currentWrapped.Process(currentToken.ToArray(), context);

						currentWrapped = null;
						currentToken.Clear();

						if (!rawEnumerator.MoveNext())
						{
							continue;
						}

						while (true)
						{
							char nextC = rawEnumerator.Current;

							if (_delimitedClosure.AdvanceDelimiter(nextC, context, out _))
							{
								break;
							}

							if (!rawEnumerator.MoveNext())
							{
								throw new ArgumentException("Unexpected closure closing (current token was not delimited after a closing character).", nameof(raw));
							}
						}
					}
					else
					{
						foreach (IWrappedClosure<TContext> closure in _wrappedClosures)
						{
							if (!closure.AdvanceOpening(c, context, out int openingTrail))
							{
								continue;
							}

							currentToken.RemoveRange(currentToken.Count - openingTrail, openingTrail);

							if (currentToken.Count > 0)
							{
								throw new ArgumentException("Unexpected closure opening (current token was not delimited after an opening character).", nameof(raw));
							}

							currentWrapped = closure;
						}

						if (currentWrapped != null)
						{
							continue;
						}

						bool delimiterFinished = _delimitedClosure.AdvanceDelimiter(c, context, out int delimiterTrail);

						currentToken.RemoveRange(currentToken.Count - delimiterTrail, delimiterTrail);

						if (!delimiterFinished)
						{
							continue;
						}

						yield return _delimitedClosure.Process(currentToken.ToArray(), context);

						currentToken.Clear();
					}
				}
			}

			if (currentWrapped != null)
			{
				throw new ArgumentException("The raw input finished with an unfinished wrapped closure.", nameof(raw));
			}

			if (currentToken.Count > 0)
			{
				yield return _delimitedClosure.Process(currentToken.ToArray(), context);
			}
		}
	}
}
