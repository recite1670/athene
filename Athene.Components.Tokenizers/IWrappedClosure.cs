namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A grouper for token characters, grouped by a start and end sequence.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public interface IWrappedClosure<in TContext> : ITokenClosure<TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	Advances the matching of the opening sequence.
		/// </summary>
		/// <param name="c">The current character of the tokenizable characters.</param>
		/// <param name="context">The currently running execution context.</param>
		/// <param name="consumed">The characters consumed when advancing the closure.</param>
		/// <returns><see langword="null"/> if it is not a complete match of the opening sequence, otherwise the count of the opening sequence.</returns>
		bool AdvanceOpening(char c, TContext context, out int consumed);

		/// <summary>
		/// 	Advances the matching of the closing sequence.
		/// </summary>
		/// <param name="c">The current character of the tokenizable characters.</param>
		/// <param name="context">The currently running execution context.</param>
		/// <param name="consumed">The characters consumed when advancing the closure.</param>
		/// <returns><see langword="null"/> if it is not a complete match of the closing sequence, otherwise the count of the closing sequence.</returns>
		bool AdvanceClosing(char c, TContext context, out int consumed);
	}
}
