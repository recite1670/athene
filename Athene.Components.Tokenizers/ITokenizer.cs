﻿using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	///     Parses raw input into a token array.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public interface ITokenizer<in TContext> where TContext : IExecutionContext
	{
		/// <summary>
		///     Parses raw input into a token array.
		/// </summary>
		/// <param name="raw">Raw input to be tokenized.</param>
		/// <param name="context">The currently running execution context.</param>
		/// <returns>A token array.</returns>
		IEnumerable<Token> Tokenize(IEnumerable<char> raw, TContext context);
	}
}
