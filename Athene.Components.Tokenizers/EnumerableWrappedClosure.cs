using System;
using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A wrapped closure that uses enumerables of characters as the opening and closing sequences.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public abstract class EnumerableWrappedClosure<TContext> : IWrappedClosure<TContext>, IDisposable where TContext : IExecutionContext
	{
		private readonly MatchableEnumerable<char> _opening;
		private readonly MatchableEnumerable<char> _closing;

		/// <summary>
		/// 	Whether or not this object has been disposed and should throw upon any calls to methods or properties aside from <seealso cref="Disposed"/>, <seealso cref="Dispose()"/>, and <seealso cref="Dispose(bool)"/>.
		/// </summary>
		protected bool Disposed { get; private set; }

		/// <summary>
		/// 	The escape character for this closure.
		/// </summary>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public char? EscapeCharacter
		{
			get
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(EnumerableWrappedClosure<TContext>));
				}

				return _opening.Escapable > 0 ? _opening.EscapeValue : (char?) null;
			}
			set
			{
				if (Disposed)
				{
					throw new ObjectDisposedException(nameof(EnumerableWrappedClosure<TContext>));
				}

				if (value.HasValue)
				{
					_opening.Escapable = 1;
					_closing.Escapable = 1;

					_opening.EscapeValue = value.Value;
					_closing.EscapeValue = value.Value;
				}
				else
				{
					_opening.Escapable = 0;
					_closing.Escapable = 0;

					_opening.EscapeValue = default;
					_closing.EscapeValue = default;
				}
			}
		}

		/// <summary>
		/// 	Constructs an instance of <see cref="EnumerableWrappedClosure{TContext}"/>.
		/// </summary>
		/// <param name="opening">The opening sequence of a token.</param>
		/// <param name="closing">The closing sequence of a token.</param>
		/// <exception cref="ArgumentNullException"><paramref name="opening"/> or <paramref name="closing"/> is <see langword="null"/>.</exception>
		protected EnumerableWrappedClosure(IEnumerable<char> opening, IEnumerable<char> closing)
		{
			if (opening == null)
			{
				throw new ArgumentNullException(nameof(opening));
			}

			if (closing == null)
			{
				throw new ArgumentNullException(nameof(closing));
			}

			_opening = new MatchableEnumerable<char>(opening);
			_closing = new MatchableEnumerable<char>(closing);
		}

		/// <summary>
		/// 	Finalizes an instance of <see cref="EnumerableWrappedClosure{TContext}"/>.
		/// </summary>
		~EnumerableWrappedClosure()
		{
			Dispose(false);
		}

		/// <inheritdoc cref="IDisposable.Dispose"/>
		/// <param name="disposing">Whether or not the method is being disposed via <seealso cref="Dispose()"/> (true) or via a finalizer (false).</param>
		protected virtual void Dispose(bool disposing)
		{
			if (Disposed)
			{
				return;
			}

			if (disposing)
			{
				_opening?.Dispose();
				_closing?.Dispose();
			}

			Disposed = true;
		}

		/// <inheritdoc cref="ITokenClosure{TContext}.Process"/>
		public abstract Token Process(IEnumerable<char> token, TContext context);

		/// <inheritdoc cref="IDisposable.Dispose"/>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <inheritdoc cref="IWrappedClosure{TContext}.AdvanceOpening"/>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public bool AdvanceOpening(char c, TContext context, out int toRemove)
		{
			if (Disposed)
			{
				throw new ObjectDisposedException(nameof(EnumerableWrappedClosure<TContext>));
			}

			return _opening.Advance(c, out toRemove);
		}

		/// <inheritdoc cref="IWrappedClosure{TContext}.AdvanceClosing"/>
		/// <exception cref="ObjectDisposedException">This object has been disposed and should not be called.</exception>
		public bool AdvanceClosing(char c, TContext context, out int toRemove)
		{
			if (Disposed)
			{
				throw new ObjectDisposedException(nameof(EnumerableWrappedClosure<TContext>));
			}

			return _closing.Advance(c, out toRemove);
		}
	}
}
