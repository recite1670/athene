using System;
using System.Collections.Generic;

namespace Athene.Components.Tokenizers
{
	/// <summary>
	/// 	A wrapped closure that executes the command within the closure.
	/// </summary>
	/// <typeparam name="TContext">The type of the execution context to be ran in.</typeparam>
	public class SubstitutionClosure<TContext> : EnumerableWrappedClosure<TContext> where TContext : IExecutionContext
	{
		/// <summary>
		/// 	Constructs an instance of <see cref="SubstitutionClosure{TContext}"/>.
		/// </summary>
		/// <param name="opening">The opening sequence of the token.</param>
		/// <param name="closing">The closing sequence of the token.</param>
		/// <exception cref="ArgumentNullException"><paramref name="opening"/> or <paramref name="closing"/> is <see langword="null"/>.</exception>
		public SubstitutionClosure(IEnumerable<char> opening, IEnumerable<char> closing) : base(opening, closing)
		{
		}

		/// <summary>
		/// 	Finalizes an instance of <see cref="SubstitutionClosure{TContext}"/>.
		/// </summary>
		~SubstitutionClosure()
		{
			Dispose(false);
		}

		/// <inheritdoc cref="EnumerableWrappedClosure{TContext}.Process"/>
		public override Token Process(IEnumerable<char> token, TContext context)
		{
			if (Disposed)
			{
				throw new ObjectDisposedException(nameof(SubstitutionClosure<TContext>));
			}

			CommandResult result = context.Execute(token);

			switch (result.Outcome)
			{
				case ResultOutcome.Fail:
					throw new CommandException("Substitution command failed: " + result.Message);

				case ResultOutcome.NotFound:
					throw new CommandException("Substitution command not found.");

				case ResultOutcome.Success:
					return new Token(result.Message, false);

				default:
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}
