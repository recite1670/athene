using System;
using System.Collections.Generic;
using System.Linq;
using Athene.Components.Interpolators;
using Athene.Components.Tokenizers;
using Athene.Tables;

namespace Athene.Components.Contexts
{
	/// <summary>
	/// 	A component-bound console that can execute console commands.
	/// </summary>
	/// <typeparam name="TContext">An <see cref="IExecutionContext"/> that this type implements to be passed to components.</typeparam>
	public abstract class ComponentExecutionContext<TContext> : IExecutionContext where TContext : IExecutionContext
	{
		/// <summary>
		/// 	The tokenizer for this context.
		/// </summary>
		protected ITokenizer<TContext> Tokenizer { get; }

		/// <summary>
		/// 	The commands for this context.
		/// </summary>
		protected IAliasTable<ICommand<TContext>> Commands { get; }

		/// <summary>
		/// 	The interpolator for this context.
		/// </summary>
		protected IInterpolator<TContext> Interpolator { get; }

		/// <summary>
		/// 	This context in generic-type form.
		/// </summary>
		protected abstract TContext GenericSelf { get; }

		/// <summary>
		/// 	Constructs an instance of <see cref="ComponentExecutionContext{TContext}"/>.
		/// </summary>
		/// <param name="commands">The commands for this context.</param>
		/// <param name="tokenizer">The tokenizer for this context.</param>
		/// <param name="interpolator">The interpolator for this context.</param>
		/// <exception cref="ArgumentNullException"><paramref name="tokenizer"/> or <paramref name="commands"/> is <see langword="null"/>.</exception>
		protected ComponentExecutionContext(IAliasTable<ICommand<TContext>> commands, ITokenizer<TContext> tokenizer, IInterpolator<TContext> interpolator)
		{
			Tokenizer = tokenizer ?? throw new ArgumentNullException(nameof(tokenizer));
			Commands = commands ?? throw new ArgumentNullException(nameof(tokenizer));
			Interpolator = interpolator;
		}

		/// <summary>
		///		Executes a tokenized command.
		/// </summary>
		/// <param name="alias">The alias of the command.</param>
		/// <param name="tokens">The tokens of the command, not including the alias.</param>
		/// <exception cref="ArgumentNullException"><paramref name="alias"/> or <paramref name="tokens"/> is <see langword="null"/>.</exception>
		protected CommandResult ExecuteTokenized(string alias, IEnumerator<string> tokens)
		{
			if (alias == null)
			{
				throw new ArgumentNullException(nameof(alias));
			}

			if (tokens == null)
			{
				throw new ArgumentNullException(nameof(tokens));
			}

			ICommand<TContext> command = Commands[alias];
			if (command == null)
			{
				return CommandResult.NotFound;
			}

			try
			{
				return CommandResult.Succeed(command.Execute(tokens, GenericSelf));
			}
			catch (CommandException e)
			{
				return CommandResult.Fail(e.Message);
			}
			catch (Exception e)
			{
				return CommandResult.Fail($"Unhandled exception executing command:{Environment.NewLine}{e}");
			}
		}

		/// <summary>
		/// 	The preconditions of <see cref="Execute"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="input"/> is <see langword="null"/>.</exception>
		protected virtual void ExecutePreconditions(IEnumerable<char> input)
		{
			if (input == null)
			{
				throw new ArgumentNullException(nameof(input));
			}
		}

		/// <summary>
		/// 	The body of <see cref="Execute"/> without any preconditions.
		/// </summary>
		protected virtual CommandResult ExecuteBody(IEnumerable<char> input)
		{
			IEnumerable<Token> tokens = Tokenizer.Tokenize(input, GenericSelf);
			IEnumerable<string> interpolatedTokens;

			if (Interpolator != null)
			{
				string InterpolateToken(Token token)
				{
					IEnumerable<char> value = token.Interpolate ? Interpolator.Interpolate(token.Value, GenericSelf) : token.Value;

					return new string(value.ToArray());
				}

				interpolatedTokens = tokens.Select(InterpolateToken);
			}
			else
			{
				string CoalesceToken(Token token)
				{
					return new string(token.Value.ToArray());
				}

				interpolatedTokens = tokens.Select(CoalesceToken);
			}

			using (IEnumerator<string> tokensEnumerator = interpolatedTokens.GetEnumerator())
			{
				return !tokensEnumerator.MoveNext() ? CommandResult.Fail("The input contained no tokens.") : ExecuteTokenized(tokensEnumerator.Current, tokensEnumerator);
			}
		}

		/// <inheritdoc cref="IExecutionContext.Execute"/>
		/// <returns>A <see cref="CommandResult" /> representing the success and message to show the user.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="input" /> is <see langword="null"/>.</exception>
		public CommandResult Execute(IEnumerable<char> input)
		{
			List<char> inputList = input.ToList();

			ExecutePreconditions(inputList);

			return ExecuteBody(inputList);
		}
	}

	/// <summary>
	/// 	A component-bound console that can execute console commands with no additional data.
	/// </summary>
	public sealed class ComponentExecutionContext : ComponentExecutionContext<IExecutionContext>
	{
		/// <inheritdoc cref="ComponentExecutionContext{TContext}.GenericSelf"/>
		protected override IExecutionContext GenericSelf => this;

		/// <summary>
		/// 	Constructs an instance of <see cref="ComponentExecutionContext"/>.
		/// </summary>
		/// <param name="commands">The commands for this context.</param>
		/// <param name="tokenizer">The tokenizer for this context.</param>
		/// <param name="interpolator">The interpolator for this context.</param>
		/// <exception cref="ArgumentNullException"><paramref name="tokenizer"/> or <paramref name="commands"/> is <see langword="null"/>.</exception>
		public ComponentExecutionContext(IAliasTable<ICommand<IExecutionContext>> commands, ITokenizer<IExecutionContext> tokenizer, IInterpolator<IExecutionContext> interpolator = null) : base(commands, tokenizer, interpolator)
		{
		}
	}
}
